#!/usr/bin/perl -w

use Pod::Usage;
use Getopt::Long;
use strict;

my($opt_help, $opt_man);

GetOptions(
	'help' => \$opt_help,
	'man'  => \$opt_man,
)
or pod2usage( "Try '$0 --help' for more information.");

pod2usage( -verbose => 1 ) if $opt_help;
pod2usage( -verbose => 2 ) if $opt_man;

pod2usage(1) if (-t);

$/="\n>";

while (my $entry=<STDIN>)
  {
    chomp $entry;
    $entry=~s/^>//;
    $entry=~s/(\S+).*?\n//;
    my $name=$1;
	$entry=~s/\n//g;
	printf ("%s\t%d\n",$name,length($entry));
  }

=pod

=head1 NAME

 fasta_length.pl

=head1 SYNOPSIS

 fasta_length.pl < file.fa

=head1 DESCRIPTION

 Read a fasta file as STDIN, print entry name and entry length as STDOUT

=head1 AUTHORS

 Patrice Dehais

=head1 VERSION

 1

=head1 DATE

 2013

=head1 KEYWORDS

 fasta length

=head1 EXAMPLE

 cat file.fa | fasta_length.pl

=cut

