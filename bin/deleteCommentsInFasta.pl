#!/usr/bin/perl 
use strict;
use warnings;
use Pod::Usage;
use Getopt::Long;

#------------------------
# main
main:
{

 	# options hash table
	my %h_options;
			
 	Getopt::Long::Configure( "no_ignorecase" );
 	Getopt::Long::Configure( "bundling" );
	# retrieve all options
 	GetOptions (\%h_options, 
		'h|help', 
		'fasta_file|d=s',
		'output_file|o=s'
	) or  pod2usage( -verbose => 1 , noperldoc => 1);
	# help 
	pod2usage( -verbose => 1 , noperldoc => 1) if (exists $h_options{'h'});
	# mandatory options
	foreach my $opt ( 'fasta_file', 'output_file')
	{
		if (not exists $h_options{ $opt })
		{
			pod2usage( 
				-verbose 	=> 1,
				-noperldoc	=> 1,
				-msg		=> "Missing mandatory option $opt " 
			);
		}
	}
my $fasta_file	= $h_options{ 'fasta_file' } ;
my $output_filename = $h_options{ 'output_file' } ;
open ( OUTPUT , ">". $output_filename ) or die "Can't open file $output_filename : $!";
open ( INPUT , $fasta_file ) or die "Can't open file $fasta_file : $!";
while (my $line = <INPUT> )
{
	if ( ($line =~ /^>/ ) )
	{
		my @line_sep = split(' ', $line);
		print OUTPUT $line_sep[0] . "\n";
	}
	else
	{
		print OUTPUT $line;
	}
}
close (INPUT);
close (OUTPUT);
}

=pod

=head1 NAME

deleteCommentsInFasta.pl

=head1 SYNOPSIS

deleteCommentsInFasta.pl [OPTIONS]

=head1 DESCRIPTION

Delete comments in fasta header

=head1 OPTIONS
		
	-h,--help
		Help (with more details )
		
	-d,--fasta_file file
		fasta_file		
		
	-o,--output_file file
		output file

=head1 VERSION

1

=head1 DATE

08 2013

=head1 KEYWORD

fasta header

=head1 AUTHORS

Claire Hoede

=cut

