#!/usr/bin/perl -w
# read a fasta sequence file as stdin and write as stdout a two columns 
# tab delimited text file with accetion numbre as first column and sequence
# nucleotides gathered in one string as second column

use strict;

$/="\n>";
while(my $fasta=<STDIN>)
  {
    $fasta=~s/^>//;
    chomp $fasta;
    my @Lines=split(/\n/,$fasta);
    my $ac=shift @Lines;
    $ac=~/^(\S+)/;
    printf "$1\t%s\n",join('',@Lines);
  }

=pod

=head1 NAME

fasta2raw.pl

=head1 SYNOPSIS

fasta2raw.pl < my_file.fasta

=head1 DESCRIPTION

Reads a fasta sequence file as stdin and write as stdout a two columns 
tab delimited text file with accetion numbre as first column and sequence 
nucleotides gathered in one string as second column

=head1 AUTHORS

Patrice Dehais

=head1 VERSION

1

=head1 DATE

18/03/2013

=head1 KEYWORDS

fasta converstion tab

=head1 EXAMPLE

fasta2raw.pl < STDIN > STDOUT

=cut

