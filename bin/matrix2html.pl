#!/usr/bin/perl
use strict;
use warnings;
use Pod::Usage;
use Getopt::Long;
use File::Basename;



sub header () {
    print << "EOF";
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<link rel="shortcut icon" type="image/ico" href="" />
		
		<title>Sigenae/Genotoul miRNA pipeline</title>
		<style type="text/css" title="currentStyle">
			
			\@import "http://snp.toulouse.inra.fr/~sigenae/LIB/css/demo_page.css";
                	\@import "http://snp.toulouse.inra.fr/~sigenae/LIB/css/demo_table_jui.css";
			\@import "http://snp.toulouse.inra.fr/~sigenae/LIB/css/jquery-ui-1.8.4.custom.css";
		</style>
		<script type="text/javascript" language="javascript" src="http://snp.toulouse.inra.fr/~sigenae/LIB/js/jquery.js"></script>
		<script type="text/javascript" language="javascript" src="http://snp.toulouse.inra.fr/~sigenae/LIB/js/jquery.dataTables.js"></script>
		<script type="text/javascript" charset="utf-8">
			var asInitVals = new Array();
			
			\$(document).ready(function() {
				var oTable = \$('#example').dataTable( {
					"bJQueryUI": true,
                                        "sScrollX": "100%",
                                        "sScrollXInner": "110%",
  		                        "bScrollCollapse": true,
					"sPaginationType": "full_numbers",
                                        "oLanguage": {
						"sSearch": "Search all columns:"
					}
				} );
				
				\$("tfoot input").keyup( function () {
					/* Filter on the column (the index) of this element */
					oTable.fnFilter( this.value, \$("tfoot input").index(this) );
				} );
				
				
				
				/*
				 * Support functions to provide a little bit of 'user friendlyness' to the textboxes in 
				 * the footer
				 */
				\$("tfoot input").each( function (i) {
					asInitVals[i] = this.value;
				} );
				
				\$("tfoot input").focus( function () {
					if ( this.className == "search_init" )
					{
						this.className = "";
						this.value = "";
					}
				} );
				
				\$("tfoot input").blur( function (i) {
					if ( this.value == "" )
					{
						this.className = "search_init";
						this.value = asInitVals[\$("tfoot input").index(this)];
					}
				} );
			} );
		</script>
	</head>
	<body id="dt_example">
		<div id="container">
			<div id="demo">
<table cellpadding="0" cellspacing="0" border="0" class="display" id="example">

EOF
}
  
sub footer () {
  print << "EOF";
</table>
			</div>

		</div>
	</body>
</html>
EOF
}



# ------------------------
# main
main : {
  # options hash table
  my %h_options;

  Getopt::Long::Configure( "no_ignorecase" );
  Getopt::Long::Configure( "bundling" );

  # retrieve all options
  GetOptions (\%h_options,
	      'h|help'
	     ) or  pod2usage( -verbose => 1 , noperldoc => 1) ;
	
  # help
  pod2usage( -verbose => 1 , noperldoc => 1) if (exists $h_options{'h'});
	
  # test ARGV parameters
  pod2usage( -verbose => 2 , noperldoc => 1) if (@ARGV != 1);

  header();

  my $cmp = 0;
  my @a_header;
  open (MAT, $ARGV[0]) or die "Can't open file $ARGV[0] : $!";
  while (my $line=<MAT>) {
    chomp $line;
    $cmp++;
    my @a_line = split('\t', $line);
    if($line =~ /^#/) {
      print "<thead><tr>\n";
      foreach my $l (@a_line) {
	print "\t<th>$l</th>\n";
	push(@a_header, $l);
      }
      print "</tr></thead>\n";
      print "<tbody>\n";
    }
    else {
      print "<tr>\n";
      foreach my $l (@a_line) {
	print "\t<td>$l</td>\n";
      }
      print "</tr>\n";
    }
    last if($cmp > 10000);
  }
  close MAT;

  print "</tbody>\n<tfoot>\n<tr>\n";
  foreach my $l (@a_header) {
    print "\t<th><input type=\"text\" name=\"$l\" value=\"Search\" class=\"search_init\" /></th>\n";
  }
  print "</tr>\n</tfoot>\n";

  footer();
}

=pod

=head1 NAME

matrix2html.pl

=head1 SYNOPSIS

matrix2html.pl FILE.CSV > FILE.HTML
        
=head1 DESCRIPTION

Build html report (using jquery dataTables) from tab separated file         

        Input tab separated file sample:
        #col1  col2  col3[...]
        val1   val2  val3[...]
        [...]

        Warning:
        - First line must be start by #
        - Only the first 10000 lines of the input file will be used

=head1 OPTIONS

-h,--help

=head1 AUTHORS

Philippe Bardou

=head1 VERSION

1

=head1 DATE

2013

=head1 KEYWORDS

Matrix HTML Visualisation

=cut
