#! /usr/bin/perl
# $Id$

=pod

=head1 NAME

 extractPartsFromRef.pl

=head1 DESCRIPTION

 Displays sub-sequences from a (multi-)fasta file according to start/stop positions defined by user.
 The used coordinate system is one-based, i.e. first nucleotide of the region has index 1. 

=head1 SYNOPSIS

 extractPartsFromRef.pl FASTA REGIONS_TO_EXTRACT [-f OUT_FORMAT]

=head1 ARGUMENTS

 1-    Path to (multi-)fasta file from which sub-sequences will be extracted.
 2-    Path to the file containing the regions to extract according to the following format
       WARNING : the regions must NOT be overlapping.
       SEQUENCE NAME<tab>FIRST POSITION<tab>LAST POSITION

=head1 OPTIONS

 -f    Output format : Fasta, EMBL, GenBank, swiss, SCF ... (cf. 
       Bio::SeqIO) [Defaut : Fasta].

=head1 DATE

 21/05/2013

=head1 VERSION

 1.1

=head1 KEYWORDS

 extraction fasta

=head1 EXAMPLE

 extractPartsFromRef.pl myGenome.fasta myRegionsOfInterest.txt

=head1 AUTHORS

 Escudie Frederic - Plateforme genomique Toulouse (get-plage.bioinfo@genotoul.fr)

=head1 DEPENDENCIES

 Pod::Usage
 Bio::Seq
 Bio::SeqIO

=cut


#############################################################################################################################
#
#		LIBRAIRIES
#
#############################################################################################################################
use strict ;
use warnings ;
use Getopt::Long ;
use Pod::Usage ;
use Bio::Seq ;
use Bio::SeqIO ;


#############################################################################################################################
#
#		FONCTION PRINCIPALE
#
#############################################################################################################################
MAIN:
{
	my %selected_regions = () ;
	my $reference_file   = $ARGV[0] ;
	my $regions_file     = $ARGV[1] ;
	my $output_format    = 'Fasta' ;
	my $help             = 0 ;

	#Gestion des paramètres
	GetOptions(
	            "format|f=s" => \$output_format,
	            "help|h"     => \$help
	);

	if( $help || @ARGV != 2 )
	{
		pod2usage(
		           -sections => "SYNOPSIS|ARGUMENTS|DESCRIPTION|VERSION",
		           -verbose => 99
		) ;
	}

	checkParam( $reference_file, $regions_file ) ;

	#Récupérer les régions à extraire
	open(FH_REGIONS, $regions_file) ;

	while( <FH_REGIONS> )
	{
		my $line = $_ ;
		chomp($line) ;
		my ($region, $start, $stop) = split(/\t/, $line) ;
		$selected_regions{$region}{$start} = $stop ;
	}

	close FH_REGIONS ;

	#Créer le format de sortie
	my $output = Bio::SeqIO->new('-format' => $output_format) or die "Format de sortie non valide.\n" ;

	#Récupérer les régions en parcourant la référence
	my $region_name   = "" ;
	my $pos_on_region = 1 ;
	my $seq = "" ;
	my $start = -1 ;
	my $extraction = 0 ;

	open(FH_REFERENCE, $reference_file) ;

		#Pour chaque ligne du fichier
		while( <FH_REFERENCE> )
		{
			my $line = $_ ;
			chomp($line) ;

			#Si on est sur une nouvelle région de la référence (ex : nouveau chromosome)
			if( $line =~ /^>([^\s]+)/ )
			{
				$region_name = $1 ;
				$pos_on_region = 0 ;
				$start = -1 ;
				$extraction = 0 ;
			}
			#Si on est sur une ligne de séquence
			else
			{
				my @seq_section = split(//, $line) ;
				my $pos_on_section = 0 ;

				while( $pos_on_section < @seq_section )
				{
					$pos_on_region++ ;

					if( defined($selected_regions{$region_name}{$pos_on_region}) )
					{
						$seq = "" ;
						$start = $pos_on_region ;
						$extraction = 1 ;
					}

					if( $extraction )
					{
						$seq .= $seq_section[$pos_on_section] ;

						if( $pos_on_region == $selected_regions{$region_name}{$start} )
						{
							#Afficher la séquence au format souhaité
							my $seqObj = Bio::Seq->new(
							         -seq => $seq,
                                 			         -id  => $region_name.'_'.$start.'_'.$pos_on_region
						        );
							$output->write_seq($seqObj);
							
							#Réinitialiser pour la séquence suivante
							$seq = "" ;
							$extraction = 0 ;
						}
					}

					$pos_on_section++ ;
				}
			}
		}

	close FH_REFERENCE ;
}


#############################################################################################################################
#
#		SOUS-FONCTIONS
#
#############################################################################################################################

=head2 procedure checkParam

 Title        : checkParam
 Usage        : checkParam( $reference_file, $regions_file )
 Prerequisite : none
 Function     : Teste la valeur des paramètres utilisateur.
 Returns      : none
 Args         : $reference_file   String - Chemin du fichier fasta.
                $regions_file     String - Chemin du fichier csv indiquant les 
                                  positions des séquences d'intérêt.
 Globals      : none

=cut

sub checkParam
{
	my ( $reference_file, $regions_file ) = @_ ;
	my $error_msg = "" ;

	#Si le fasta de référence n'est pas fournit
	if( $reference_file eq "" )
	{
		$error_msg .= "[ERREUR] Le fasta de référence doit être fourni.\n" ;
	}
	#Sinon
	else
	{
		my $msg = checkReadableFile($reference_file) ;
		if( $msg )
		{
			$error_msg .= "[ERREUR] ".$msg ;
		}
	}
	
	#Si le fichier indiquant les positions des séquences d'intérêt n'est pas fournit
	if( $regions_file eq "" )
	{
		$error_msg .= "[ERREUR] Un fichier indiquant les positions des séquences d'intérêt doit être fournit.\n" ;
	}
	#Sinon
	else
	{
		my $msg = checkReadableFile($regions_file) ;
		if( $msg )
		{
			$error_msg .= "[ERREUR] ".$msg ;
		}
	}

	#Si il existe une ou plusieurs erreurs
	if( $error_msg ne "" )
	{
		die $error_msg."Pour plus d'information tapez : ".$0." -help.\n" ;
	}
}


=head2 function checkReadableFile

 Title        : checkReadableFile
 Usage        : checkReadableFile( $file_path )
 Prerequisite : none
 Function     : Retourne un message d'erreur si le fichier n'existe pas, n'est 
                pas un fichier standard, ou que l'utilisateur n'a pas les 
                droits de lecture.
 Returns      : "" ou le message d'erreur.
 Args         : $file_path     String - Chemin du fichier à tester.
 Globals      : none

=cut

sub checkReadableFile
{
	my ( $file_path ) = @_ ;
	my $msg = "" ;

	#Si le fichier n'existe pas
	if( !(-e $file_path) )
	{
		$msg .= "Le fichier ".$file_path." n'existe pas.\n" ;
	}
	#Sinon si le fichier n'est pas un fichier standard
	elsif( !(-f $file_path) )
	{
		$msg .= $file_path." n'est pas un fichier.\n" ;
	}
	#Sinon si le fichier n'est pas accessible en lecture
	elsif( !(-f $file_path) )
	{
		$msg .= "Vous n'avez pas les droits nécessaires pour lire le fichier ".$file_path.".\n" ;
	}
	
	return $msg ;
}
