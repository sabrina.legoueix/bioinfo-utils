#!/usr/local/bioinfo/bin/python2.5

from Bio import SeqIO
import sys
import string
import math
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
from optparse import *

__name__ = "length_histogram.py"
__synopsis__ = "length_histogram.py -i fastafile"
__date__ = "2012/06"
__authors__ = "Celine Noirot"
__keywords__ = "fasta histogram length"
__description__ = "This script generate a histogram of the length of the sequences."
__version__ = '1.0'

parser = OptionParser(usage="Usage: length_histogram.py -i FILE -l")
parser.add_option("-i", "--in", dest="fasta_file",
                  help="The fasta file", metavar="FILE")
parser.add_option("-l", "--log", action="store_true", dest="log",
                  help="Log10 x axis")
(options, args) = parser.parse_args()

if options.fasta_file == None :
    parser.print_help()
    sys.exit(1)
else :

    x = []
    if options.log == None :
        for seq_record in SeqIO.parse(open(options.fasta_file, "rU"), "fasta") :
            x.append(len(seq_record.seq.tostring()))
    else :
        for seq_record in SeqIO.parse(open(options.fasta_file, "rU"), "fasta") :
            x.append(math.log(len(seq_record.seq.tostring()),10))

    plt.hist(x, 50, facecolor='green', alpha=0.75)
    plt.grid(True)
    plt.savefig(options.fasta_file+".lengthhist.png")

    sys.exit(0)
