#!/usr/bin/perl -w
use strict;
use Getopt::Long;
use Pod::Usage;

# Options definitions (name,format,default,required)
my @Options = ( 
	['help',undef,undef,0],
	['man',undef,undef,0],
);
# defaults values
my %Values=();
my %Required=();
map {$Values{$_->[0]}=$_->[2];$Required{$_->[0]}=$_->[3];} @Options;
# build options list
my %Options=();
map {$Options{defined($_->[1])?$_->[0].'='.$_->[1]:$_->[0]}=\$Values{$_->[0]};} @Options;
# retrieve options
GetOptions(%Options) || pod2usage(2);
pod2usage(1) if ($Values{'help'});
pod2usage(-exitstatus => 0, -verbose => 2) if ($Values{'man'});

# check required
map {pod2usage("$_ is required") if ($Required{$_}&&!defined($Values{$_}));} keys %Values;

$#ARGV>=0 || pod2usage("need as parameter(s) accession numbers file list, or accession numbers themselve");

# read wanted accession numbers
my %AC=();
if (open(FIN,$ARGV[0])) {
  while (my $line=<FIN>) {
    chomp $line;
    $AC{$line}=undef;
  }
  close(FIN);
}
else {
  map{$AC{$_}=undef;} @ARGV;
}

#------------------------------------------------------------
sub read_fastq ( )
  {
    # read sequence header
    my $line='';
    while ($line=<STDIN>) {
      last if ($line=~/^@/);
      next if ($line=~/^\s*$/);
    }
    return undef unless (defined($line));
    my %fq=();
    chomp $line;
    $line=~/^@(\S+)/ || die "not a fastq valid file: $line";
    $fq{ac}=$1;
    $fq{def}=$line;
    $fq{seq}=$fq{qual}='';
    # loop on sequence lines
    while ($line=<STDIN>) {
      next if ($line=~/^\s*$/);
      last if ($line=~/^\+/);
      chomp $line;
      $fq{seq}.=$line;
    }
    defined($line) || die "bad formated fastq file: can't locate quality for AC=$fq{ac}";
    my $lg1=length($fq{seq});
    my $lg2=0;
    while($lg2<$lg1 && defined($line)) {
      $line=<STDIN>;
      defined($line) || die "bad formated fastq file: incomplete quality for AC=$fq{ac}";
      next if ($line=~/^\s*$/);
      chomp $line;
      $fq{qual}.=$line;
      $lg2+=length($line);
    }
    $lg1==$lg2 || die "bad formated fastq file: quality and sequence lengthes differ for AC=$fq{ac}";
    return \%fq;
  }

#------------------------------------------------------------

# loop on fastq entries
while (my $fq=read_fastq()) {
  if(exists($AC{$fq->{ac}})) {
    printf "%s\n%s\n+\n%s\n",$fq->{def},$fq->{seq},$fq->{qual};
  }
}

#============================================================

=head1 NAME

fastq_extract.pl

=head1 SYNOPSIS

fastq_extract.pl [options] <ACs|AC_file> < file.fastq

=head1 OPTIONS

=over 8

=item B<-help>

Print a brief help message and exits.

=item B<-man>

Prints the manual page and exits.

=back

=head1 INPUTS

=over 8

=item B<ACs|AC_file>

List of accession numbers to be extracted, or a file containing this list

=item B<file.fastq>

The fastq file given as standard input (STDIN)

=back

=head1 DESCRIPTION

 Read a FASTQ file as STDIN
 If parameter 1 is a file, consider it as a list of accession numbers,
 otherwise consider all paramters as accession numbers of sequences
 to be extrated from the FASTQ file

=head1 AUTHORS

 Patrice Dehais

=head1 VERSION

 1

=head1 DATE

 2013

=head1 KEYWORDS

 fastq extract

=head1 EXAMPLE

 cat all.fastq | fastq_extract.pl some.lst > some.fastq

=cut
