#!/usr/bin/perl

#---------------------------#
#  Author:  Sylvain Marthey #
#  Project: mitomiR         #
#  Release: 1.0             #
#  Date :   01/01/2008      #
#---------------------------#
#
#       csfastq_to_csfasta.pl
#
#       Copyright 2012 Sylvain Marthey <sylvain.marthey@jouy.inra.fr>
#       
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, see <http://www.gnu.org/licenses/>.


use strict;
use Getopt::Long;
use Pod::Usage;

my $help;

GetOptions("help|?" => \$help) 
  or
  pod2usage(-message=>"Try `$0' for more information.", -exitval => 2,-verbose=> 0);
pod2usage(-exitval =>1, -verbose => 2) if ($help);

if((!$ARGV[0] && !-f($ARGV[0]))){
  print "parameters missing : try csfastq_to_csfasta.pl <input file path>\n";
  exit;
}

open (IN , $ARGV[0]);
my @size;
# on r�cup�re les noms
while (<IN>) {
	$_ =~ s/^@/>/;
	print $_;
	my $seq = <IN>;
	print $seq;
	<IN>;
	<IN>;
	$size[length($seq)]++;
}


=head1 NAME

 csfastq_to_csfasta.pl

=head1 SYNOPSIS

 csfastq_to_csfasta.pl  <file_path> 
 
=head1 OPTIONS

=over

=item INPUT

 name of the input fastq file.

=item HELP

 Print this usage message.

=back

=head1 DESCRIPTION

  This script will read the FASTQ file passed as input and return only the rows corresponding to the colorspace.
  
  Some software like CutAdapt produces csfastq file (fastq in colorspace), and it can be sometimes necessary to convert these files csfasta

           
=head1 AUTHORS

Sylvain Marthey <sylvain.marthey@jouy.inra.fr>

=head1 VERSION

1.1

=head1 DATE

2012

=head1 KEYWORDS

colorspace, csfasta,

=head1 EXAMPLE

csfastq_to_csfasta.pl my_reads.cut.csfastq > my_reads.cut.csfasta

=cut
