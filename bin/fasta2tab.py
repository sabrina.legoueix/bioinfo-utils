#!/bin/env python

from Bio import SeqIO
import sys
import string
import argparse

__name__ = "fasta2tab.py"
__synopsis__ = "fasta2tab.py -f file_name.fasta"
__example__ = "python fasta2tab.py file_name.fasta > STDOUT"
__date__ = "2012/10"
__authors__ = "Christophe Klopp"
__keywords__ = "fasta tablurar text format"
__description__ = "This script transforms a fasta file in a tabular format file which can sorted on the sequence"
__version__ = '1.0'

parser=argparse.ArgumentParser(description=__description__)
parser.add_argument('-f', action='store', dest='fasta_file',  help='name of the fasta file', required=True)
parser.add_argument('--version', action='version', version='%(prog)s 0.0')
args=parser.parse_args()

# sequence processing
for seq_record in SeqIO.parse(open(args.fasta_file), "fasta") :
	print seq_record.seq.tostring()+"\t"+seq_record.id



