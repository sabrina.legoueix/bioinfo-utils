#!/usr/bin/perl -I../lib


BEGIN
{
        my ($dir,$file) = $0 =~ /(.+)\/(.+)/;
        unshift (@INC,"$dir/../lib");
}


use strict;
use warnings;
use ParamParser;
use Bio::Seq;
use Bio::SeqIO;


MAIN:
{
        # L
        my $o_param = New ParamParser('GETOPTLONG',"contig=s","output=s","list=s", "not_in_list");
        $o_param->SetUsage(my $usage=sub { &Usage(); } );

	$o_param->AssertFileExists('contig');
	
	$o_param->SetUnlessDefined('output',$o_param->Get("contig").".fasta");
	$o_param->SetUnlessDefined('not_in_list',0);
	$o_param->SetUnlessDefined('list',$o_param->Get("contig").".lst");
	$o_param->AssertFileExists('list');

	my %h_list;
	my $not_in_list = $o_param->Get('not_in_list');
	open (LST, $o_param->Get('list') );
	my $line;
	while ( $line=<LST> )
	{
		#print $line;
		chomp $line;
		$line=~s/"//g;
		$h_list{$line}=1;
	}
	close (LST);
	
	print "Read list done\n";
        open (OUT, ">".$o_param->Get("output")) or die("Erreur decriture >".$o_param->Get("output")."<\n");
        # read fasta file
        my $seq = Bio::Seq->new();
        my $in = Bio::SeqIO->new( -file => $o_param->Get("contig"), '-format' => 'Fasta' );
	
        while( $seq = $in->next_seq())
        {
		my $id=$seq->id();
		$id=~s/"//g;
		if ( $not_in_list == 0 ) # traitement classique
		{
			if ( defined($h_list{$id}) )
			{
				print OUT ">".$id."\n";
				print OUT $seq->seq()."\n";
				
			}
        }

		else # on ne prends que le id qui ne sont pas ds la liste
		{
			if ( ! defined($h_list{$id}) )
			{
				print OUT ">".$id."\n";
				print OUT $seq->seq()."\n";
				
			}
		}
		
		}
       close (OUT);
}

=item Usage

        $o_param->Usage();
        Print the usage of the program

=cut

sub Usage
{
print STDERR<<END
Usage:$0
Print in output file the sequence which id is in "contig" file

[Mandatory]
        --contig 	filename     contig fasta file
	--list		filename     file of list of id to extract
[Optionnal]
        --help                       this message
        --output        filename     output file
		--not_in_list	flag		 invert the selection, take only sequence not in list
END
}
