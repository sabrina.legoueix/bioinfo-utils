#!/usr/local/bioinfo/bin/python2.5

#
# fastq_extract : Copyright (C) 2009 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Plateforme bioinformatique Midi Pyrenees'
__copyright__ = 'Copyright (C) 2009 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0'
__email__ = 'support.genopole@toulouse.inra.fr'

import os, sys, gzip, string
from Bio import SeqIO
from optparse import *


def version_string ():
    """
    Return the fastq_extract version
    """
    return "fastq_extract " + __version__

if __name__ == "__main__":

    parser = OptionParser(usage="Usage: fastq_extract.py -i FILE [options]")
    usage = "usage: %prog -i fastq_file -f fasta_file -q qual_file"
    desc = " Extract fastq file into fasta and qual files \n"\
           " ex : fastq_extract.py -i file.fastq -f file.fasta -q file.qual"
    parser = OptionParser(usage = usage, version = version_string(), description = desc)
    igroup = OptionGroup(parser, "Input files options","")
    igroup.add_option("-i", "--in", dest="input_file",
                      help="The file to clean, can be [sff|fastq|fasta]", metavar="FILE")
    parser.add_option_group(igroup)
    ogroup = OptionGroup(parser, "Output files options","")
    ogroup.add_option("-f", "--fasta", dest="fasta",
                      help="The output fasta file", metavar="FILE")
    ogroup.add_option("-q", "--qual", dest="qual",
                      help="The output quality file", metavar="FILE")
    parser.add_option_group(ogroup)
    (options, args) = parser.parse_args()
    
    if options.input_file != None :
    
        base = string.rstrip(options.input_file, '.fastq')
        if options.fasta == None:
            fasta = base + ".fasta"
        else : fasta = options.fasta
        if options.qual == None:
            qual = base + ".qual"
        else : qual = options.qual
        
        seqs = []
        if options.input_file.endswith(".gz"):
            for record in SeqIO.parse(gzip.open(options.input_file), "fastq") :
                seqs.append(record)        
        else :
            for record in SeqIO.parse(open(options.input_file), "fastq") :
                seqs.append(record)
    
        # First write down seqs
        ffile = open(fasta, "w")
        SeqIO.write(seqs, ffile, "fasta")
        ffile.close()
        
        # Write down the qual file for cross_match if possible
        qfile = open(qual, "w")
        SeqIO.write(seqs, qfile, "qual")
        qfile.close()
        
        sys.exit(0)
        
    else :
        parser.print_help()
        sys.exit(1)
        