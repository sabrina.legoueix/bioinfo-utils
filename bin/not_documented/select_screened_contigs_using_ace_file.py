#!/usr/local/bioinfo/bin/python2.5

import re, os, sys, datetime, glob, time
from optparse import *
from shutil import copyfile
from lib.Utils import Utils
from Bio import SeqIO
from Bio.Sequencing import Ace

def printContigSort(ace_file, min_depth, max_depth):
    depth = {}
    selected_contig = {}

    for contig in Ace.read(open(ace_file, 'r')).contigs:
        read_sum=0
        for read in contig.reads:
            read_sum=read_sum+len(read.rd.sequence)
        depth[contig.name] = float(read_sum/contig.nbases)
        #print "profondeur de " + str(float(read_sum/contig.nbases)) + "(longueur du contig : " + str(contig.nbases) + " pb, somme des longueurs des lectures : " + str(read_sum) + " pb avec " + str(len(contig.reads)) + " de lectures assembl&eacute;es.)"
        if int(depth[contig.name]) > min_depth and int(depth[contig.name]) < max_depth :
            #print "selected contig = "+contig.name
            selected_contig[contig.name] = contig.name

    return selected_contig

if __name__ == '__main__':
    
    parser = OptionParser(usage="Usage: filter_assembly2ng6.py -i ace_file ...")
    parser.add_option("-i", "--ace-files", dest="ace_files", help="The ace file", metavar="FILE")
    parser.add_option("-x", "--min-depth", dest="min_depth", help="The minimal depth", metavar="int")
    parser.add_option("-y", "--max-depth", dest="max_depth", help="The maximal depth", metavar="int")
    parser.add_option("-s", "--screen-files", dest="screen_files", help="The cross_match screen result file", metavar="FILE")
    (options, args) = parser.parse_args()
    
    if options.ace_files==None or options.screen_files == None or options.min_depth == None or options.max_depth == None :
        parser.print_help()
        exit(1)
    else:
	out = "./"
        
        # storing the screened contigs in a hash table
        handle = open(options.screen_files, "r")
        screen_contigs = {}
        for contig in SeqIO.parse(handle, "fasta"):
	    #print contig
	    screen_contigs[contig.name]=contig
            #print screen_contigs[contig.name]

        # Let's get the html of each analyze
        contigs = printContigSort (options.ace_files, int(options.min_depth), int(options.max_depth))
        for contig in contigs:
            print ">"+screen_contigs[contig].name
            print screen_contigs[contig].seq.tostring()
        
        #sys.exit(0)            
