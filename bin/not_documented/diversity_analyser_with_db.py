#!/usr/local/bioinfo/bin/python2.5

#
# diversityAnalyser 
# Copyright (C) 2009 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Plateforme bioinformatique Midi Pyrenees'
__copyright__ = 'Copyright (C) 2009 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'beta'

#should be done as preprocess
#def processPyrolog(pyrolog, cause):
#    """
#    @param pyrolog : filename of the pyrocleaner log to parse
#    @return        : list of removed duplicated sequences
#    """
#    del_list = []
#    go_on = True
#    for line in open(pyrolog,'r'):
#        if go_on or line.startswith("#"):
#            continue
#        elif line.startswith("## Start Basic cleaning"):
#            go_on = False
#        elif line.startswith("## header"):
#            go_on = False
#        else:#found a deleted EST
#            cur_sequence, status, arrow, cur_cause = line.split()[:4]
#            if cur_cause == cause:
#                du_list.append(cur_sequence)
#    return del_list


def requestDB(request,host,user,passwd,db):
    conn = connect(host, user, passwd, db)
    
    curs = conn.cursor() #select name, est_name, library_name from acatenellanr_sigenaecontig__contig__main a, acatenellanr_sigenaecontig__est_mrna__dm b where a.contig_id_key = b.contig_id_key limit 20;  
    curs.execute(request)
          
    return curs.fetchall()
    
def plotAll(title, tables, filename, graphStep, csvoutput):
    """Plot all conditions depth tables, takes an dict of table depth"""
    #tables is a dict {condition:[y_value]}
    plt.clf()
    palette=["blue","#00AAAA","red","magenta","#008080","#808000","green","cyan","#4B0082","#B00000","#808080","#8B4513","#98FB98","#C71585","#FFA500","#FFD700","#000080"]
    plt.subplot(111)
    maxX=0 ; maxY=0 ; j=0
    lines_to_plot = []
    labels = []

    conditions = tables.keys()
    conditions.sort()
    
    if csvoutput:
        coordsToWrite = {}
        handle_log = open(filename[:-4]+'.csv','w')
        ##find greater "t" here -> fill the others
        greater_t = ''
        gt_t_len = 0
        for key in conditions:
            t = [0]+tables[key]
            if len(t) > gt_t_len :
                gt_t_len = len(t)
                greater_t = key
        greater_table = tables[greater_t]  

    for key in conditions:
        j+=1 ; t=[0]+tables[key]
        
        maxX=max(maxX,len(t)*graphStep) 
        maxY=max(maxY,max(t))
        
        #x = arange(0, max(t), graphStep) 
        x=range(len(t))
        for i in range(len(t)):
            x[i]=x[i]*graphStep
        y=t
        if csvoutput:                        
            coords_y = y + [ '' for i in range(gt_t_len - len(tables[key]))] #fill table with blanks -> suppress shifting
            abscissa = [i*graphStep for i in range(gt_t_len)]        
            for i in range(gt_t_len):
                length = abscissa[i]
                if not coordsToWrite.has_key(length): #index on the first column
                    coordsToWrite[length] = [] 
                coordsToWrite[length].append(str(coords_y[i]))
                
        lines_to_plot.append( plt.plot(x,y,palette[j%len(palette)],label=key) )
        labels.append(key)
    #plt.axis([0, maxX, 0, maxY+graphStep])
    # plot reference y = x
    ref_limit=min(maxX,maxY)
    lines_to_plot.append( plt.plot(range(ref_limit),range(ref_limit),"black",label="Reference") )#y=x
    labels.append("Reference")
    font_size = "small"
    if len(tables.keys())>len(palette):
        font_size = "xx-small"
    font_prop = font_manager.FontProperties(stretch="ultra-condensed", size=font_size)

    plt.legend(tuple(lines_to_plot), tuple(labels), loc='lower right', ncol=1, mode=None, shadow=True, fancybox=True, prop = font_prop)
    plt.xlabel("Sequences")
    plt.ylabel("Contigs")
    plt.title(title)
    plt.axis([0, maxX*1.2, 0, maxY*1.01]) #plt.axis([xmin,xmax,ymin,ymax])
    plt.savefig(open(filename,'w'))
    
    if csvoutput:
        if len(conditions)==2: #only 1 bank, confronted to herself without singlets
            header = "EST_total_length\tContig_length_all\tContig_length_non_singlet"
        else:
            heads = []
            for i in conditions:
                if i.endswith("_without-singlet_"):
                    heads.append(i[:-17])
                else:
                    heads.append(i)
            heads.sort(cmp)
            header = "EST_total\t"+"\t".join(heads)
        handle_log.write(header)
        x_list = coordsToWrite.keys()
        x_list.sort(cmp)
        for x in x_list:
            handle_log.write( "\n"+str(x)+"\t"+"\t".join(coordsToWrite[x]))
            
def getInfo(outputDir, verbose, species, depthLimit, csvoutput, host,user,passwd,db, graphStep,checkpoint, est_ignore):
    """
    @param outputDir  : output directory, where the bitmaps are generated then tarballed
    @param verbose    : print progression ticks and step of process into stdout
    @param tableMain  : table of annotations about contigs
    @param tablemRNA  : table of EST linking to the contig name and condition name 
    @param host       : host of the database
    @param user       : user of the database
    @param passwd     : user's password to the database
    @param db         : the database's name
    @param graphStep  : sampling gap on the number of sequences (EST) lower it is, sharper will be the plot
    @param checkpoint : number of lines between ticks, used only in verbose mode
    @return           : execution status
    Comptage par contig du nombre d'ESTs des differentes conditions :
    - soit donnees par des fichiers
    - soit donnees par les nom d'EST 
    sortie: dictionnaire cle=contig valeur=table des comptes"""
    if not graphStep:
        graphStep=100 #abscissa step for plotting
    if not checkpoint:
        checkpoint=28000 #prompt a dot for each 28 000 lines
    
    conditions=[] #table of the condition names
    contigs={} #{contig: (est, condition)
    condCont={} #{cond:[cont]} list of contigs per condition
    condSumSeq={}#{cond:TotalSeqSum} sequences occurences per condition
    condToPlot={}#{cond:[SeqSum]} list of values to plot foreach condition
    os.path.curdir=outputDir
    if outputDir[-1]!='/':
        outputDir+='/' #directory must finish by a /
    lost_sequences = [] #collect all sequences wrongly fetched
        
    if verbose:    start=time.time() ;    print "Connection to database...", ;sys.stdout.flush()
  
    ### bcp trop long de faire
    #ignore_list = ""
    #if est_ignore:
    #    ignore_list = " AND ( est_name = '" + "' OR est_name = '".join(est_ignore) + "') "

    tableMain = species+"_sigenaecontig__contig__main "
    tablemRNA = species+"_sigenaecontig__est_mrna__dm"
    command = "SELECT name, est_name, library_name FROM "+tableMain+" a,"+tablemRNA+" b WHERE a.contig_id_key = b.contig_id_key"
    if depthLimit:
        command += " AND depth > "+str(depthLimit)
    command += ";"
    datas = requestDB(command, host, user, passwd, db)
        
    if verbose:    step1=time.time() ;    print "done in",step1-start,"seconds\nWrite and unsort data...", ;sys.stdout.flush()
    
  # TODO: write sequences in a tmp contigs file, unsort it and process as before    
    contigSeqTmp=NamedTemporaryFile(mode='w+b', prefix='tmp')
    
    file_size = 0
    dict_ignore = {}
    dict_ignore.fromkeys(est_ignore)
    for line in datas:
        tmp_l = []
        if dict_ignore.has_key( line[1] ):
            continue #ignore EST marked
        for i in range(len(line)):
            word = line[i]
            if word==None:
                word = "Other"
            tmp_l.append(word)
        contigSeqTmp.write("\t".join(tmp_l)+'\n')
        file_size+=1

    unsort(contigSeqTmp.name)
        
    if verbose:    step2=time.time() ;    print "done in",step2-step1,"seconds\nParsing...", ;sys.stdout.flush()
        
    checkpoint = file_size/10
    ptr=0
    contigSeqTmp.seek(0)
    for line in contigSeqTmp.file:

        ptr+=1
        if verbose and ptr%checkpoint==0:    sys.stdout.write('-')
        try:
            contig , EST, condition = (line.replace('\n','')).split('\t')
            if depthLimit:
                condition+="_without-singlet_"
        except:
            lost_sequences.append(line) #hope this don't occur too often...
        #condCont={cond:{cont:int}} ;condSumSeq={cond:TotalSeqSum}       
        if not condCont.has_key(condition):#new condition
            condSumSeq[condition]=1 #dict of total number of seq foreach condition
            condCont[condition]={contig:1} #dict listing contigs for each condition
        elif not condCont[condition].has_key(contig):#cond has new contig
            condSumSeq[condition]+=1
            condCont[condition][contig]=1      
        else: #both cond and cont are kown
            condSumSeq[condition]+=1
            condCont[condition][contig]+=1
            
        #calculations for the plot condToPlot={cond:[SeqSum]}
        if condSumSeq[condition]%graphStep==0: #step reached
            if condToPlot.has_key(condition):
                condToPlot[condition].append(len(condCont[condition]))
            else: #new dict entry and new list
                condToPlot[condition]=[len(condCont[condition])]
        sys.stdout.flush()
       
    # Remove the very low banks
    for condition in condToPlot.keys():
        if condToPlot[condition][-1]<1000:
            if condition.endswith("_without-singlet_"):
                cond = condition[:-17]
            else:
                cond = condition
            if verbose:
                print "\n",cond,"\tis not enought large, size:",condToPlot[condition][-1]
            else:
                print cond
            if condToPlot.has_key(condition):
                condToPlot.pop(condition)
    if verbose:         sys.stdout.write("\n") ; sys.stdout.flush()
    return condToPlot

def plotChooser(allToPlot, nsToPlot, graphStep):
    """
    index: condition_name ; value: list of total sequences length sum
    @param    allToPlot    dict    all contigs (even singletons)
    @param    nsToPlot    dict    contigs non-singlets
    @param graphStep  : sampling gap on the number of sequences (EST) lower it is, sharper will be the plot
    @return    0    int    just call plotting functions
    """
    if not graphStep:
        graphStep=100 #abscissa step for plotting    
    curves_per_plot = 14 #limit of curves per plot, non strict #warning for color redundancy !
    
    for cond in allToPlot.keys(): # Plot condition per condition
        title=cond
        filename=os.path.join(os.path.curdir,cond+".PNG")
        cond_ns = cond+"_without-singlet_"
        try:
            plotDict = { cond:allToPlot[cond] , cond_ns:nsToPlot[cond_ns] }
        except KeyError:
            if verbose:
                print "the condition",cond,"has only singlets or no sequences"
            continue
            
        plotAll(title,plotDict, filename, graphStep, csvoutput)
    dicts = {"all_":allToPlot, "_without-singlet_":nsToPlot}
    for dataset_name in dicts.keys(): #Plot all non singlets and all contigs separatly
        condToPlot = dicts[dataset_name]
        if len(condToPlot.keys())<curves_per_plot and len(condToPlot.keys())>1:
            plotAll("Graphe total",condToPlot, os.path.join(os.path.curdir,dataset_name+"global.PNG"),graphStep, csvoutput)
            
        elif len(condToPlot.keys())>1: #when there is too much conditions: split the data to be plot
    #        def getQuantile(table,quantity=2):
    #            """@param quantity: 2 for median, 10 for decils..."""
    #            if len(table)%quantity==0:
    #                return len(table)/quantity
    #            else:
    #                return (len(table)+1)/quantity
            def split_list(liste, size):
                """split the given list by the given size. return a list of lists"""
                if len(liste)<size:
                    return [liste]
                else:
                    return [liste[0:size]]+split_list(liste[size:], size)
            
            dict_size = {} # max_abscissa : condition
            for key in condToPlot.keys():
                t=condToPlot[key]
                if not dict_size.has_key(max(t)): #hope this never occurs twice -> FIXME recursive function | list of conditions
                    dict_size[max(t)] = key
                else :
                    dict_size[max(t)+1] = key
            list_size = dict_size.keys()
            list_size.sort()
            
            #median = getQuantile(list_size, len(list_size)/curves_per_plot)  
            list_of_list_of_size = split_list(list_size, curves_per_plot)
    
            #catenate the last list in the previous one if it's too small
            if len(list_of_list_of_size[-1])<4:
                last = list_of_list_of_size.pop()
                list_of_list_of_size[-1] += last
            
            #split dict of data to be plotted
            i=1
            for list_size in list_of_list_of_size:
                tmp_dict = {}
                for size in list_size:
                    cond = dict_size[size]
                    tmp_dict[cond] = condToPlot[cond]
                plotAll("Global "+dataset_name+" graph "+str(i)+"/"+str(len(list_of_list_of_size)), tmp_dict, os.path.join(os.path.curdir,dataset_name+"global"+str(i)+".PNG"),graphStep, csvoutput)
                i+=1
    return 0

def diversity_depts_error():
    """
    Return the list of dependencies missing to run the diversityAnalyzer
    """
    error = ""
    if not which("unsort") == None:
        error += " - unsort is missing"
    if not which("MySQLdb") == None:
        error += " - MySQLdb is missing"
    if not which("matplotlib") == None:
        error += " - matplotlib is missing"
    if error != "":
        error = "Cannot execute diversityAnalyzer.py, following dependencies are missing :\n" + error + " Please install them!"
    return error
def which (program):
    """
    Return if the asked program exist in the user path
      @param options : the options asked by the user
    """
    def is_exe(fpath):
        return os.path.exists(fpath) and os.access(fpath, os.X_OK)
    fpath, fname = os.path.split(program)
    if fpath:
        if is_exe(program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return exe_file
    return None

if __name__ == "__main__":
    
    import re, os, time, sys, glob, warnings
    from Bio import SeqIO
    from tempfile import NamedTemporaryFile
    from optparse import OptionParser, OptionGroup
    import matplotlib.pyplot as plt
    from matplotlib import font_manager
    import matplotlib.lines as lines
    from unsort import unsort
    warnings.simplefilter("ignore")

    from MySQLdb import *
    
    parser = OptionParser(usage="usage: ./diversityAnalyzer.py -r directory [-cv -d 1] -e speciesname -s hostname -b DBname -u username -p password")
    parser.description="This program analyze the diversity of a set of sequences: for each condition, it will plot the number of contigs if function of the number of sequences. Resulting pictures are in PNG and archived in the result directory."
    
    required=OptionGroup(parser, "Required options")
  # output directory 
    required.add_option("-r", "--results", dest="resultDirectory",
                        help="write the bitmaps results in DIRECTORY", metavar="DIRECTORY")
    
  # database connection parameters 
    required.add_option("-s", "--host", dest="host",
                        help="name of the database host", metavar="NAME")
    required.add_option("-u", "--user", dest="user",
                        help="name of the database user", metavar="NAME")
    required.add_option("-p", "--password", dest="password",
                        help="password of the given user for this database", metavar="PASSWORD")
    required.add_option("-b", "--base", dest="database",
                        help="name of the database", metavar="NAME")
    required.add_option("-e", "--species", dest="species",
                        help="name of the studied species (as noted in database for the tables names)", metavar="NAME")
  
  # optional options 
    facultative=OptionGroup(parser, "Facultative options")
    facultative.add_option("-c", "--csvoutput",action="store_true", dest="csvoutput", default=False,
                           help="output in results directory all csv of used coordinates for the plots")     
    #facultative.add_option("-d", "--depthlimit", dest="depthLimit",
    #                       help="depth limit (strictly superior to this value)", metavar="INT")    
    facultative.add_option("-l", "--pyrocleanerlog", dest="pyrolog",
                           help="parsed pyrocleaner log: list of duplicates", metavar="FILE")
    facultative.add_option("-v", "--verbose", action="store_true", dest="verbose", default=False,
                           help="print all status messages to stdout")   
    facultative.add_option("-g", "--graphical_step", dest="graphStep",
                           help="graphical step for the result bitmaps", metavar="INTEGER")
    facultative.add_option("-t", "--tickspace", dest="checkPoint",
                           help="(if verbose) is the number of sequences between two dots of the progression bar", metavar="INTEGER")
    facultative.add_option("--demo", action="store_true", dest="demo", default=False,
                           help="Demonstration on alexandrium dataset")
    parser.add_option_group(required) ; parser.add_option_group(facultative)
    
    (options, args) = parser.parse_args()
    
    if not diversity_depts_error():
        outputDir = options.resultDirectory ; verbose = options.verbose 
        graphStep = options.graphStep ; checkPoint = options.checkPoint
        
        host = options.host ; user = options.user ; passwd = options.password
        db = options.database ; species = options.species
        
        csvoutput = options.csvoutput
        #depthLimit = options.depthLimit
        pyrolog = options.pyrolog
        demo = options.demo
        ###demo=True

        if demo:
            ##For tests
            verbose = True
            host="intron.toulouse.inra.fr"
            user="sigenae"
            passwd="s1i2g3!"
            db="sig_lel_ensembl_mart_40_1"
            species = "acatenellanr"
            depth = None
            outputDir="./"
            verbose=True
            csvoutput=True

        list_of_deleted = []

        if not outputDir or not host or not user or not passwd or not db or not species:
            print"Please give all the required fields"
            parser.print_help()
            sys.exit(1)
        #depts and args ok, launch the program
        elif pyrolog:
            if os.path.exists(pyrolog):
                #processPyrolog(pyrolog, "Duplicated")
                ##TODO: need preprocess of the pyrocleaner logfile : give here a list of contig id
                for i in open(pyrolog,'r').readlines(): ##
                    list_of_deleted.append( i.replace('\n','') )

        if not os.path.exists(outputDir):
            os.makedirs(outputDir, 0755)
        beginnig = time.time()
        #launch a 1st time for non singlet
        if verbose:    print "\n\tnon singlet processing"
        non_singlets_dict = getInfo(outputDir, verbose, species, 1, csvoutput, host, user, passwd, db, graphStep, checkPoint, list_of_deleted)
        #rename non signlet files
        for file in glob.glob(os.path.join(outputDir,'*')):
            extension = file[-4:]
            if extension == ".PNG" or extension == ".csv":
                rootfilename = file[:-4]
                os.rename(file, rootfilename+"_without-singlet_"+extension)
        if verbose:    print "\n\tall contigs processing"
        # 2nd time : singlets
        all_contigs_dict = getInfo(outputDir, verbose, species, 0, csvoutput, host, user, passwd, db, graphStep, checkPoint, list_of_deleted)        
        
        if verbose:    print "Generating all bitmaps"
        plotChooser(all_contigs_dict, non_singlets_dict, graphStep)
        
        if verbose:    print "Archiving into ",os.path.join(outputDir,"results.tar.gz")
        ##TODO: use built-ins functions of python and gzip !  
        all_csv = ''
        if csvoutput:
            all_csv = '*.csv'
        os.system("cd "+outputDir+" ; tar -Pc *.PNG "+all_csv+" --remove-files | gzip -c > results.tar.gz")
        if verbose:
            print "\nJob ended in",time.time()-beginnig,"seconds"
    else:
        print diversity_depts_error()