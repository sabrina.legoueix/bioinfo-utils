#!/usr/bin/perl -I../lib
=head1 NAME

Blast2seq2m8.pl - Programme qui converti une sortie bl2seq au format m8 de blast

=head1 SYNOPSIS

Blast2seq2m8.pl --input input_file [ --output output_file]

=head1 DESCRIPTION

This program convert a standard output of bl2seq in a m8 blast format
m8 blast format is a tabular file with followin colums :
Query id, Subject id, % identity, alignment length, mismatches, gap openings, q. start, q. end, s. start, s. end, e-value, bit score
If output is not defined then output file will be input file suffixed by .m8

=cut




BEGIN
{
        my ($dir,$file) = $0 =~ /(.+)\/(.+)/;
        unshift (@INC,"$dir/../lib");
}

use strict;
use ParamParser;
use Bio::AlignIO;
MAIN:
{
    
    my $o_param = New ParamParser('GETOPTLONG',"input=s","output=s");
	$o_param->SetUsage(my $usage=sub { &Usage(); } );
	$o_param->AssertFileExists('input');
	my $input = $o_param->Get('input');
	$o_param->SetUnlessDefined('output',$input.".m8");
	my $output = $o_param->Get('output');	
	
	open (IN, $input) or die "unable to open input file : $input\n";
	open (OUT, ">$output") or die "unable to open output file : $output\n";
	my ($q_first,$s_first,$q_last,$s_last)=(-1,-1,-1,-1);
	my ($query_id, $subject_id, $pct_identity, $alignment_length, $mismatches, $gap_openings, $q_start, $q_end, $s_start, $s_end, $e_val, $bit_score)=("","",-1,-1,-1,-1,-1,-1,-1,-1,-1);
	while (my $line=<IN>)
	{
		chomp ($line);
		if ($line =~ /^Query=\s/)
		{	
			($query_id)=$line =~ /^Query=\s(\S+)\s/;
		}
		if ($line =~ /^>\S+/)
		{
			($subject_id)=$line =~ /^>(\S+)/;
		}

		if ($line=~ /\sScore.*/)
		{
			if ($q_start!=-1 && $s_start!=-1)
			{
				$q_end=$q_last;
				$s_end=$s_last;
				print OUT "$query_id\t$subject_id\t$pct_identity\t$alignment_length\t$mismatches\t$gap_openings\t$q_start\t$q_end\t$s_start\t$s_end\t$e_val\t$bit_score\n";
				($q_last,$s_last)=(-1,-1);
				($pct_identity, $alignment_length, $mismatches, $gap_openings, $q_start, $q_end, $s_start, $s_end, $e_val, $bit_score)=(-1,-1,-1,-1,-1,-1,-1,-1,-1);
			}
			($bit_score, $e_val)=$line =~ /\sScore\s=\s+(\S+)\sbits\s.+,\sExpect\s=\s(.+)/;
			#print "bit_score $bit_score, e_val $e_val\n";
		}

		# Identities = 229/229 (100%)
		if ($line=~ /\sIdentities.*/)
		{
			($alignment_length, $pct_identity)=$line =~ /\sIdentities\s=\s\d+\/(\d+)\s\((\d+)%\)/;
			#print "alignment_length $alignment_length pct_identity $pct_identity\n";
		}

		if ($line=~ /Query:\s/)
		{
			($q_first,$q_last)=$line =~ /Query:\s+(\d+)\s+\S+\s(\d+)/;
			$q_start=$q_first if ($q_start==-1);
		}

		if ($line=~ /Sbjct:\s/)
		{
			($s_first,$s_last)=$line =~ /Sbjct:\s+(\d+)\s+\S+\s(\d+)/;
			$s_start=$s_first if ($s_start==-1);
		}

	}
	if ($q_start!=-1 && $s_start!=-1)
	{
		$q_end=$q_last;
		$s_end=$s_last;
		print OUT "$query_id\t$subject_id\t$pct_identity\t$alignment_length\t$mismatches\t$gap_openings\t$q_start\t$q_end\t$s_start\t$s_end\t$e_val\t$bit_score\n";
	}
	close(IN);
	close(OUT);
	system "sort -k 7,8 -n $output > $output.srt; rm $output; mv $output.srt $output";
}



=item Usage

        $o_param->Usage();
        Print the usage of the program

=cut

sub Usage
{
print STDERR<<END
Usage:$0
Programme qui converti une sortie bl2seq au format m8 de blast

[Mandatory]

    	--input			filename     	input bl2seq file
[Optionnal]
        --help                       	this message
		--output		filename		[input.m8] output file 
END
}
