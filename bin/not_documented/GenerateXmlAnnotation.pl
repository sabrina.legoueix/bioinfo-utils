#!/usr/bin/perl


=head1 NAME

GenerateXmlAnnotation.pl - Programme qui pour un fichier fasta utilise les fichiers d'analyses pour générer un GameXml pour apollo.
Les analyses doivent être stocké dans le meme repertoire que le fichier fasta.
Le fichier de configuration doit contenir les informations suivantes :
#liste des analyses
analysis=blastEst blastSwissprot gthEst
# pour chaque analyse  l'extention utilisé :
blastEst_ext=est.blast
blastSwissprot_ext=swissprot.blast
gthEst=est.gth #under development
Ce programme est capable de traiter des fichiers blast verbeux et des fichiers gff.
Nous utilisons le nom de l'analyse pour le determiner.

=head1 SYNOPSIS

GenerateXmlAnnotation.pl --fasta_file file --cfg file --output file

=head1 DESCRIPTION

=cut




BEGIN
{
        my ($dir,$file) = $0 =~ /(.+)\/(.+)/;
        unshift (@INC,"$dir/../lib");
}

use strict;
use ParamParser;
use Convert2GameXml;
use File::Glob;

MAIN:
{
    
	my $o_param = New ParamParser('GETOPTLONG',"fasta_file=s","cfg=s", "output=s");
	$o_param->SetUsage(my $usage=sub { &Usage(); } );

	$o_param->AssertFileExists('fasta_file');
	my $fasta_file = $o_param->Get('fasta_file');

	$o_param->AssertFileExists('cfg');
	$o_param->Update($o_param->Get('cfg'),'A');

	$o_param->SetUnlessDefined('output',$fasta_file.".game");
	my $output =$o_param->Get('output');

	#recupere la liste des analyse
	my @analysis=split (" ", $o_param->Get('analysis'));

	my ($dir,$file) = $fasta_file =~ /(.+)\/(.+)$/;
	my ($id)=$file=~ /(\w+)\.\w+$/; # supprime les extentions pour obtenir l'identifiant

	# création d'un objet convert
	$o_param->AssertDefined('organism');
	my $organism= $o_param->Get('organism');
	print "> $organism\n";
	my $convert = Convert2GameXml->New();	
	$convert->Initialize($fasta_file ,$output,$organism );	
	foreach my $analyse (@analysis)
	{
		print "-> Analyse : $analyse\n";
		my @files=glob $fasta_file.".*".$o_param->Get($analyse.'_ext');
                # si le fichier d'analyse est vide ou n'existe pas
                if ( $#files< 0 )
                {
                      print "Analyze: ".$analyse."  no files found :".$fasta_file.".*".$o_param->Get($analyse.'_ext')."\n";
                      next;
                }
   
		if ($analyse =~/blast/)
		{
			if (defined ($o_param->Get('min_pci')) && defined ($o_param->Get('min_len') )  && defined ($o_param->Get('max_evalue') ) )
			{
			$convert->generateComputationnalXML(\@files,"blast", $o_param->Get('min_len'), ($o_param->Get('min_pci'),$o_param->Get('max_evalue')));
			}
			else
			{
			$convert->generateComputationnalXML(\@files,"blast");
			}	
		}
		elsif ($analyse =~/gth/)
		{
			$convert->generateComputationnalXML(\@files,"gth");
		}
		else
		{
			$convert->generateComputationnalXML(\@files,"gff");
		}
	}
	# Ecriture dans le fichier ouput.
	$convert->writeOutfile();
	close(IN);
}


=item Usage

        $o_param->Usage();
        Print the usage of the program

=cut

sub Usage
{
print STDERR<<END
Usage:$0 --fasta_file fa --cfg annotation.cfg --output fa.game
Generate a gameXml file from a fasta file and all analysis generated (blast and gff) and listed in cfg file.

[Mandatory]
    	--fasta_file 		filename     	fasta file
	--cfg			filename	config file.
[Optionnal]
        --help         		this message
	--output		filename 	[fasta_file.game]
END
}
