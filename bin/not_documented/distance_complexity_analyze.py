#!/usr/local/bioinfo/bin/python2.5

from Bio import SeqIO
from optparse import *
import string
import zlib
import os
import numpy as np
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import sys
from matplotlib.ticker import NullFormatter, MultipleLocator, FormatStrFormatter

def distance (str1, str2):
        val = 0
        for i in range(0,len(str1)):
                if str1[i] != str2[i] :
                        val = val + -1
                else:
                        if str1[i] != ' ' :
                                val = val + 1
        return val

parser = OptionParser(usage="Usage: distance_complexity_analyze.py -i FILE -l length")
parser.add_option("-i", "--in", dest="fasta_file", help="The fasta file", metavar="FILE")
parser.add_option("-l", "--length", dest="length", help="the cut value")
(options, args) = parser.parse_args()

if options.fasta_file == None or options.length == None:
    parser.print_help()
else :
    file = open(options.fasta_file+".txt", 'w')
    for reads_record in SeqIO.parse(open(options.fasta_file), "fasta"):
        if len(reads_record) > int(options.length):
            file.write(str(reads_record.seq)[:int(options.length)]+"\t"+reads_record.id+"\n")
    file.close()

    cmd = "more "+options.fasta_file+".txt | sort > "+options.fasta_file+".txt.sorted"
    os.system(cmd) 

    x = []
    y = []
    fhandle = open(options.fasta_file+".txt.sorted")
    file = open(options.fasta_file+".stats", 'w')
    indic = 0
    for line in fhandle:
            if indic > 0 :
                    string = line[:-1].split("\t")[0]
                    name = line[:-1].split("\t")[1]
                    dist = distance(refstring,string)
                    zstr1 = zlib.compress(string)
                    x.append(dist)
                    y.append(len(zstr1))
                    file.write(name + "\t" + str(dist) + "\t" + str(len(zstr1))+"\n")
                    refstring = string
                    refname = name
            else :
                    refstring = line[:-1].split("\t")[0]
                    refname = line[:-1].split("\t")[1]
            indic += 1
    file.close()
            
    # Then plot the result
    # definitions for the axes
    left, width = 0.1, 0.65
    bottom, height = 0.1, 0.65
    bottom_h = left_h = left+width+0.02
    rect_scatter = [left, bottom, width, height]
    rect_histx = [left, bottom_h, width, 0.2]
    rect_histy = [left_h, bottom, 0.2, height]
    nullfmt = NullFormatter() # no labels
    minorLocator = MultipleLocator(0.1)
    # start with a rectangular Figure
    plt.figure(1, figsize=(12,12))
    axScatter = plt.axes(rect_scatter)
    axHistx = plt.axes(rect_histx)
    axHisty = plt.axes(rect_histy)
    # no labels
    axHistx.xaxis.set_major_formatter(nullfmt)
    axHisty.yaxis.set_major_formatter(nullfmt)
    axScatter.scatter(x, y)
    # now determine nice limits by hand:
    binwidth = 0.25
    xmax = np.max(np.fabs(x))
    xmin = -xmax 
    ymax = np.max(np.fabs(y))
    ymin = np.min(np.fabs(y))
    axScatter.set_xlim( (xmin, xmax) )
    axScatter.set_ylim( (ymin, ymax) )
    xbins = np.arange(xmin, xmax + binwidth, binwidth)
    ybins = np.arange(ymin, ymax + binwidth, binwidth)
    xhist = axHistx.hist(x, bins=xbins)
    axHisty.hist(y, bins=ybins, orientation='horizontal')
    axHistx.set_xlim( axScatter.get_xlim() )
    axHisty.set_ylim( axScatter.get_ylim() )
    plt.savefig(options.fasta_file+".png")
