#!/usr/local/bioinfo/bin/python2.5
# -*- coding: utf-8 -*-
#
# Counting 
# Copyright (C) 2009 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Plateforme bioinformatique Midi Pyrenees'
__copyright__ = 'Copyright (C) 2009 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'beta'

def strTab(table):
    for i in range(len(table)):
        table[i]=str(table[i])
    return '\t'.join(table)

def parseDB(request,host,user,passwd,db):
    """
    Fetch table of results for the request on host:db by user
    """
    conn = connect(host, user, passwd, db)
    
    curs = conn.cursor() #select name, est_name, library_name from acatenellanr_sigenaecontig__contig__main a, acatenellanr_sigenaecontig__est_mrna__dm b where a.contig_id_key = b.contig_id_key limit 20;  
    curs.execute(request)
          
    return curs.fetchall()
    
def counting(output, verbose, species, host, user, passwd, db, est_ignore, annotation, tableProt):
    """(contigs, params)
    params : condition experiences
    Comptage par contig du nombre d'ESTs des différentes conditions :
    - soit donnees par des fichiers
    - soit donnees par les nom d'EST 
    sortie: dictionnaire cle=contig valeur=table des comptes"""
    checkpoint=28000 #prompt a dot for each 28 000 lines
    conditions=[] #table of the condition names
    contigs={} #{contig: (est, condition)
        
    if verbose:    start=time.time() ;    print "\nConnection to database...", ;sys.stdout.flush()
    tableMain = species+"_sigenaecontig__contig__main"
    tablemRNA = species+"_sigenaecontig__est_mrna__dm"
    
    
    datas = parseDB("SELECT name, est_name, library_name FROM "+tableMain+" a,"+tablemRNA+" b \
                     WHERE a.contig_id_key = b.contig_id_key AND depth>1;",
                     host,user,passwd,db)
       
    if not annotation :
        tmp = parseDB("SELECT * FROM "+tableMain+" WHERE depth>1 ;",host,user,passwd,db)
      # annotation dict {contig_name : annotations}
        annotationData = {}
        for line in tmp: #line[0] is an index
            annotationData[line[1]] = line[2:]
    else:
        tmp = parseDB("SELECT b.name, a.* FROM "+tableProt+" a , "+tableMain+" b WHERE a.contig_id_key = b.contig_id_key AND protein_hit_database = \""+annotation+"\" AND protein_hit_is_best_bool = 1 ;",host,user,passwd,db)
      # annotation dict {contig_name : annotations}
        annotationData = {}
        for line in tmp: #line[0] is an index
            annotationData[line[0]] = line[3:-1] #skip contig_name, contig_id, annot_base and bool protein_hit_database
                
#    for i in range(10):
#        print tmp[i]
#    
#    
    if verbose:    step1=time.time() ;    print "done in",step1-start,"seconds\n_\nProcessing...", ;sys.stdout.flush()

    redundantListConditions=[]    
  # list all conditions:  
    for i in datas:
        redundantListConditions.append(i[2])
    conditions = list( set( redundantListConditions ) )    #print datas[:10:]
    conditions.sort()
    print "conditions", conditions

    dbHead=[]
    if not annotation :
        tmp = parseDB("explain "+tableMain+";",host,user,passwd,db)
    else:
        tmp = parseDB("explain "+tableProt+";",host,user,passwd,db)
    for i in tmp:
        dbHead.append(i[0])
    if not annotation:
        dbHead.append("0") # add a useless value to be deleted xD
    
    topAnnots = dbHead[2:-1] #remove useless columns like contig-id, contig_name and boolean
    
    
  # write output header
    result = open(output,'w')
    header = 'Contig_name'
    for c in conditions:
        if c == None:
            c = "Other"
        header+= '\t'+c


    result.write(header+'\t'+'\t'.join(topAnnots) ) #add annotations...

  # initialization for parsing
    current = ""
    table = []
    for i in conditions:
        table.append(0)

    if verbose:    ptr=0

    #if we want to ignore some est (because of pyrocleaner, for example)
    dict_ignore = {}
    if est_ignore:
        dict_ignore = dict_ignore.fromkeys(est_ignore)

    for line in datas: #Looping
        if verbose:
            ptr+=1
            if ptr%checkpoint==0:
                sys.stdout.write('.')

        contig,EST,condition = line#[:3]
        #annotations = line[3:]

        if dict_ignore.has_key( EST ):
            continue #ignore EST marked
      
      # ignore header
      # if contig=="name":    continue

        if current!=contig: #new block
            if current!="": #NOT at the beginning
                
                #rebuild the depth column
                if annotation:
                    table.append(sum(table)) #add as last element the total depth
                    
                if not annotationData.has_key(current):
                    result.write('\n'+current+'\t'+strTab(table))
                else:
                    result.write('\n'+current+'\t'+strTab(table+list(annotationData[current])))
                #result.write(annotationData[current])#from contig_main
                #result.write('\n')
            #then, initialize
            current=contig ; table=[]   #\
            for i in conditions:        # reinitialization
                table.append(0)         #/
        
        for i in range(len(conditions)):
            if condition==conditions[i]:
                try:
                    table[i]+=1
                except IndexError:
                    print "\n\nIndex error on: table",table, "est",EST,"contig",contig,"condition",condition
    if not annotationData.has_key(current):
        result.write('\n'+current+'\t'+strTab(table))
    else:                    
        result.write('\n'+current+'\t'+strTab(table+list(annotationData[current]))) #write the last one
    result.close()
    if verbose:    print'\n_\nend, total time: ',time.time()-start,"seconds"
    return 0

if __name__ == "__main__":
    import re
    import os
    from Bio import SeqIO
    from tempfile import NamedTemporaryFile
    from optparse import OptionParser, OptionGroup
    import time
    import sys
    import datetime
    from MySQLdb import *

    parser = OptionParser(usage="usage: %prog -s hostname -u username -p userpwd -d dbname -e species -r filename")
    parser.description="This program counts the contig occurences in the given conditions:\nContigs\tCond1\tCond2\t...\tAnnotations\ncontig1\tint\tint\t...\tannotations of the contig1\n\nUsing the parameters file to identify the condition's sequences and contig_seq file formatted as follow:"
    
    required=OptionGroup(parser, "Required options")
    required.add_option("-r", "--results", dest="resultFile",
                        help="write the result matrix in FILE", metavar="FILE")
    required.add_option("-s", "--host", dest="host",
                        help="name of the database host", metavar="NAME")
    required.add_option("-u", "--user", dest="user",
                        help="name of the database user", metavar="NAME")
    required.add_option("-p", "--password", dest="password",
                        help="password of the given user for this database", metavar="PASSWORD")
    required.add_option("-d", "--database", dest="database",
                        help="name of the database", metavar="NAME")
    required.add_option("-e", "--species", dest="species_name",
                        help="name of the studied species (as noted in database for the tables names)", metavar="NAME")
#    required.add_option("-c", "--contigs_main", dest="tableMain",
#                        help="name of the table of contigs annotations", metavar="NAME")
#    required.add_option("-m", "--contigs_mrna", dest="tablemRNA",
#                        help="name of the table of contigs - mRNA linking", metavar="NAME")
    facultative=OptionGroup(parser, "Facultative options")
    facultative.add_option("-l", "--pyrocleanerlog", dest="pyrolog",
                       help="pyrocleaner log to parse for duplicates detection", metavar="FILE")
    facultative.add_option("-v", "--verbose",
                           action="store_true", dest="verbose", default=False,
                           help="print all status messages to stdout")   
    facultative.add_option("-a", "--annotation", dest="annot",
                           help="name of the database to be used for annotation", metavar="NAME")
    facultative.add_option("--demo", action="store_true", dest="demo", default=False,
                           help="Demonstration on alexandrium dataset")
        
    parser.add_option_group(required) ; parser.add_option_group(facultative)
    
    (options, args) = parser.parse_args()
    #contigs=options.contigFile ; parameters=options.paramFile ; annotations=options.annotFile
    results=options.resultFile ; verbose=options.verbose ; host = options.host
    user = options.user ; passwd = options.password ; db = options.database
    species = options.species_name #tableMain = options.tableMain ; tablemRNA = options.tablemRNA
    annot = options.annot
    demo = options.demo    
    pyrolog = options.pyrolog
    
    if demo:
        ##For tests on local machine
        results = "./acatenellanr_testDB.csv" ; verbose = True
        host="intron.toulouse.inra.fr"
        user="sigenae"
        passwd="s1i2g3!"
        db="sig_lel_ensembl_mart_40_1"
        species = "acatenellanr"
        #annot = "swissprot"
        #tableMain = "acatenellanr_sigenaecontig__contig__main "
        #tablemRNA = "acatenellanr_sigenaecontig__est_mrna__dm"
    
    list_of_deleted = []
    
    if not results or not host or not user or not passwd or not db or not species:
        print"Please give all the required fields"
        parser.print_help()
        sys.exit(1)
        
    tableProt = None
    if annot:
        IS_IN_DATABASE = None # ? TODO:
        tableProt = species+"_sigenaecontig__protein_hit__dm"
        request = "SELECT DISTINCT(protein_hit_database) FROM "+tableProt+" ;"
        availables_banks = parseDB(request,host,user,passwd,db)
        # convert to a list
        availables_banks = [str(i[0]) for i in availables_banks] 
        
        if not availables_banks.__contains__(annot):
            print "The required bank", annot ,"is not available or misspelled. Availables are :"
            print ", ".join(availables_banks),"\nTry again..."
            sys.exit(1)
            
    if pyrolog:
        if os.path.exists(pyrolog):
            for i in open(pyrolog,'r').readlines(): ##processPyrolog(pyrolog, "Duplicated")
                list_of_deleted.append( i.replace('\n','') )
                
    counting(results, verbose, species, host, user, passwd, db, list_of_deleted, annot, tableProt)

                                
    
    