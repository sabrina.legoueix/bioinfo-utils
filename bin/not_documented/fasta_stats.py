
#
# fasta_stats : Copyright (C) 2009 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Plateforme bioinformatique Midi Pyrenees'
__copyright__ = 'Copyright (C) 2009 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0'
__email__ = 'support.genopole@toulouse.inra.fr'

import os
import math
from Bio import SeqIO
from optparse import *
import zlib
import sys
import re
import glob
import numpy as np
def version_string ():
    """
    Return the fasta_stats version
    """
    return "fasta_stats " + __version__


def fasta_stats (options):
    """
    compute stats on input seqs by length, ns and complexity
      @param seqs    : table of seqs to filter   
      @param options : the options asked by the user
    """
    
    reads_length = []
    reads_ns = []
    reads_complexity = []
    # Go throught all sequences
    for i, reads_record in enumerate(SeqIO.parse(open(options.input_file), "fasta")) :
        reads_length.append(len(reads_record))
        reads_ns.append((float(reads_record.seq.count("n")+reads_record.seq.count("N"))/float(len(reads_record)))*float(100))
        reads_complexity.append((float(len(zlib.compress(str(reads_record.seq))))/float(len(reads_record)))*float(100))
        
    # Length stats : compute the mean and the standard deviation 
    length_mean = sum(reads_length) / len(reads_length)
    length_mq = length_mean**2
    length_s = sum([ x**2 for x in reads_length])
    length_var = length_s/len(reads_length) - length_mq
    length_etype = math.sqrt(length_var)
    log.write("## Length stats ##\n")
    log.write("mean="+str(round(length_mean))+"bp\n")
    log.write("std="+str(round(length_etype))+"bp\n")    
    log.write("median="+str(round(np.median(reads_length),2))+"bp\n")    
    log.write("max="+str(round(np.max(reads_length)))+"bp\n")   
    log.write("min="+str(round(np.min(reads_length)))+"bp\n")  
    
    # ns stats : compute the mean and the standard deviation 
    ns_mean = sum(reads_ns) / len(reads_ns)
    ns_mq = ns_mean**2
    ns_s = sum([ x**2 for x in reads_ns])
    ns_var = ns_s/len(reads_ns) - ns_mq
    ns_etype = math.sqrt(ns_var)
    log.write("## Ns stats ##\n")
    log.write("mean="+str(round(ns_mean))+"% of n\n")
    log.write("std="+str(round(ns_etype))+"% of n\n")    
    log.write("median="+str(round(np.median(reads_ns),2))+"% of n\n")    
    log.write("max="+str(round(np.max(reads_ns)))+"% of n\n")   
    log.write("min="+str(round(np.min(reads_ns)))+"% of n\n")  

    # Complexity stats : compute the mean and the standard deviation 
    complexity_mean = sum(reads_complexity) / len(reads_complexity)
    complexity_mq = complexity_mean**2
    complexity_s = sum([ x**2 for x in reads_complexity])
    complexity_var = complexity_s/len(reads_complexity) - complexity_mq
    complexity_etype = math.sqrt(complexity_var)
    log.write("## Complexity stats ##\n")
    log.write("mean="+str(round(complexity_mean))+"\n")
    log.write("std="+str(round(complexity_etype))+"\n")    
    log.write("median="+str(round(np.median(reads_complexity),2))+"\n")    
    log.write("max="+str(round(np.max(reads_complexity)))+"\n")   
    log.write("min="+str(round(np.min(reads_complexity)))+"\n")  
    

def which (program):
    """
    Return if the asked program exist in the user path
      @param options : the options asked by the user
    """
    import os
    def is_exe(fpath):
        return os.path.exists(fpath) and os.access(fpath, os.X_OK)
    fpath, fname = os.path.split(program)
    if fpath:
        if is_exe(program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return exe_file
    return None
   

if __name__ == "__main__":

    parser = OptionParser(usage="Usage: fasta_stats.py -i FILE [options]")

    usage = "usage: %prog -i file"
    desc = " Compute some statistics on the fasta sequences (Length, ns, complexity) \n"\
           " ex : fasta_stats.py -i file.fna"
    parser = OptionParser(usage = usage, version = version_string(), description = desc)

    igroup = OptionGroup(parser, "Input files options","")
    igroup.add_option("-i", "--in", dest="input_file",
                      help="The fasta file", metavar="FILE")
    parser.add_option_group(igroup)
    
    ogroup = OptionGroup(parser, "Output files options","")
    ogroup.add_option("-g", "--log", dest="log_file",
                      help="The log file name (default:fasta_stats.log)", metavar="FILE", default="fasta_stats.log")
    parser.add_option_group(ogroup)
    (options, args) = parser.parse_args()
    
    if options.input_file == None :
        parser.print_help()
        sys.exit(1)    
    else:
        global log
        log = open(options.log_file, "w")

        fasta_stats(options)

        log.close()
        sys.exit(0)
        
