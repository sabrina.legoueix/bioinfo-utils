#! /usr/bin/perl
# $Id$

=pod
=head1 NAME

 GCCoverageCorrelation.pl

=head1 DESCRIPTION

 Afficher pour chaque position d'une fenetre glissante sur le genome : la
 couverture mediane et le GC% de la fenetre.

=head1 SYNOPSIS

 GCCoverageCorrelation.pl GENOME
                          COVERAGE
                          [-w WINDOWS_SIZE]
                          [-s WINDOWS_STEP_SIZE]
                          [-d SEPARATOR]

=head1 ARGUMENTS

 1-    Fichier fasta contenant le genome de reference utilise pour generer le
       fichier de couverture.
 2-    Fichier de couverture (genomeCoverageBed).

=head1 OPTIONS

 -d, delimiter   Caratere separant chaque champ d'informations dans la sortie 
                 [Defaut : tabulation].
 -s, win_step    Taille des sauts de la fenetre glissante [Defaut : 5].
 -w, win_size    Taille de la fenetre glissante dans laquelle on calcule la 
                 couverture mediane et le GC% median [Defaut : 100].

=head1 VERSION

 1.1

=head1 DEPENDENCIES

 Aucune.

=head1 AUTHOR

 Escudie Frederic - Plateforme genomique Toulouse

=cut


#############################################################################################################################
#
#		LIBRAIRIES
#
#############################################################################################################################
use strict ;
use warnings ;
use Getopt::Long ;
use Pod::Usage ;


#############################################################################################################################
#
#		FONCTION PRINCIPALE
#
#############################################################################################################################
MAIN:
{
	my $genome       = $ARGV[0] ; #G�nome de r�f�rence utilis� pour g�n�rer le fichier de couverture
	my $coverage     = $ARGV[1] ; #Fichier de couverture (genomeCoverageBed)
	my $window_size  = 100 ;      #Taille de la fen�tre glissante dans laquelle on calcule la couverture et le GC% m�dian
	my $window_step  = 5 ;        #Taille des sauts de la fen�tre glissante 
	my $separator    = "\t" ;     #S�parateur de champs utilis� dans le CSV de sortie
	my $help         = 0 ;

	#Gestion des param�tres
	GetOptions(
	            "win_size|w=i"  => \$window_size,
	            "win_step|s=i"  => \$window_step,
	            "delimiter|d=s" => \$separator,
	            "help|h"        => \$help
	);

	if( $help || @ARGV != 2 )
	{
		pod2usage(
		           -sections => "SYNOPSIS|ARGUMENTS|OPTIONS|DESCRIPTION|VERSION",
		           -verbose => 99
		) ;
	}

	#Parcours du g�nome
	my $current_step ;
	my @current_seq_window ;
	my @current_cov_window ;
	my $seq_region = "" ;

	open(my $FH_GENOME, $genome) or die "Impossible d'ouvrir ".$genome." : ".$! ;
	open(my $FH_COVERAGE, $coverage) or die "Impossible d'ouvrir ".$coverage." : ".$! ;

	#Pour chaque r�gion
	while( <$FH_GENOME> )
	{
		my $seq_line = $_ ;
		chomp $seq_line ;

		#Si on est sur l'en-t�te d'une r�gion
		if( $seq_line =~ /^>([^\s]+)/ )
		{
			$seq_region = $1 ;
			@current_seq_window = () ;
			@current_cov_window = () ;
			$current_step = $window_step ;
		}
		#Si on est sur la s�quence
		else
		{
			my @seq_fields = split(//, $seq_line) ;

			#Pour chaque position
			foreach my $current_nucleotid (@seq_fields)
			{
				#Si � la base pr�c�dente la fen�tre glissante �tait d�j� pleine
				if( @current_cov_window == $window_size )
				{
					#Eliminer la couverture et la s�quence de la base la plus en amont
					shift(@current_seq_window) ;
					shift(@current_cov_window) ;
				}
				
				#R�cup�rer la couverture de la base courante
				my $covergare_line = <$FH_COVERAGE> ;
				chomp $covergare_line ;

				my ($cov_region, $cov_position, $coverage) = split(/\t/, $covergare_line) ;

				push( @current_cov_window, $coverage ) ;
				
				#R�cup�rer la base
				push( @current_seq_window, $current_nucleotid ) ;
				
				#Indiquer le d�calage
				$current_step++ ;
		
				#Si la fen�tre est de taille suffisante et le step atteint
				if( (@current_seq_window ==  $window_size) && ($current_step >= $window_step) )
				{
					#Calcul de la couverture
					my $median_coverage = getMedian(\@current_cov_window) ;
					#Calcul du GC%
					my $GC_percent = getGCPercent(\@current_seq_window) ;
					#Remettre le saut � z�ro
					$current_step = 0 ;
				
					print $seq_region.$separator.($cov_position-$window_size+1).$separator.$median_coverage.$separator.$GC_percent."\n" ;
				}
			}
		}
	}

	close( $FH_GENOME ) ;
	close( $FH_COVERAGE ) ;
}


#############################################################################################################################
#
#		SOUS-FONCTIONS
#
#############################################################################################################################

=head2 function getMedian

 Title        : getMedian
 Usage        : $median = getMedian( $list_ref )
 Prerequisite : none
 Function     : Retourne la m�diane d'une liste de valeurs.
 Returns      : float
 Args         : $list_ref   Array Ref - R�f�rence de la liste.
 Globals      : none

=cut

sub getGCPercent
{
	my ( $seq ) = @_ ;
	my $GC_count = 0 ;

	#Pour chaque position
	foreach my $current_base ( @{$seq} )
	{
		#Si la base est un G ou un C
		if( $current_base eq 'G' || $current_base eq 'C' )
		{
			$GC_count++ ;
		}
	}

	return( $GC_count/@{$seq} ) ;
}


=head2 function getMedian

 Title        : getMedian
 Usage        : $median = getMedian( $list_ref )
 Prerequisite : none
 Function     : Retourne la m�diane d'une liste de valeurs.
 Returns      : float
 Args         : $list_ref   Array Ref - R�f�rence de la liste.
 Globals      : none

=cut

sub getMedian
{
	my ( $list ) = @_ ;
	my @sort_list = sort {$a <=> $b} @{$list} ;
	my $nb_elts_to_median = int((@sort_list/2)+0.5) ;
	
	#Si la liste contient un nombre impair d'�l�ments
	if( (@sort_list % 2) != 0 )
	{
        	return( $sort_list[$nb_elts_to_median-1] ) ;
	}
	#Si la liste contient un nombre pair d'�l�ments
	else
	{
        	return( ($sort_list[$nb_elts_to_median-1]+$sort_list[$nb_elts_to_median])/2 ) ;
	}
}