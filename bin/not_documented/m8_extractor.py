#!/usr/local/bioinfo/bin/python2.5

from Bio import SeqIO
from optparse import *
import os, sys
    
usage = "usage: %prog -i input_file -b blast_file"
desc = " Extract sequences from fasta|qual file considering the blast result file \n"
parser = OptionParser(usage = usage, version = "1.0", description = desc)
parser.add_option("-i", "--input", dest="input_file", help="The input file", metavar="FILE")
parser.add_option("-o", "--output", dest="output_file", help="The output file", metavar="FILE")
parser.add_option("-f", "--format", dest="input_format", help="The format of the input file", type="string", default="fasta")
parser.add_option("-b", "--blast-input-list", dest="blast_input_list", help="List of blast file in m8 format", metavar="FILE")
(options, args) = parser.parse_args()

if options.input_file == None or options.blast_input_list == None or options.output_file == None:
    parser.print_help()
    sys.exit(1);
else :
    file = open(options.blast_input_list, 'r')
    ids = {}
    for line in file.readlines():
        m8 = open(line.rstrip(), 'r')
        for blast in m8.readlines():
            if not blast.startswith("#"):
                ids[blast.rstrip().split()[0]] = 1
        m8.close ()
    file.close ()

    seq_to_save = []
    for record in SeqIO.parse(open(options.input_file), options.input_format):
        if not ids.has_key(record.id):
            seq_to_save.append(record)
    
    out = open(options.output_file, "w")
    SeqIO.write(seq_to_save, out, options.input_format)
    out.close()
    sys.exit(0);
    