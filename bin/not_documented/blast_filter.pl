#!/usr/bin/perl

use strict; 
use Bio::SearchIO;
use Bio::SeqFeature::Generic;
use Bio::SeqIO::game;
use Getopt::Long;
# STDIN : blast file
# parameter in the line

MAIN:
{
   my $blast_file="";
   my $pci=90;
   my $pcq=90;
   my $outfile="";
   GetOptions ("blast_file=s" => \$blast_file, 
				"output=s" => \$outfile, 
				"pci=i" => \$pci,
				"pcq=i" => \$pcq);

	if ($blast_file eq "" && ! -e $blast_file)
	{ 
		print "Blast file is required\n"; 
		&Usage();
	}
	
	if ($outfile eq "")
	{ 
		print "Output file is required\n"; 
		&Usage();
	}

	
	my $seq= new Bio::Seq();
	my $in = new Bio::SearchIO(-format => 'blast', -file => $blast_file); 
	open ( OUT, ">".$outfile);
	my $database_name;
	my $nb_contamination=0;
	while( my $result = $in->next_result ){
		my $query_length = $result->query_length();
		$database_name = $result->database_name();
		
		my $print_header=0;
		while( my $hit = $result->next_hit ){
			while( my $hsp = $hit->next_hsp ){
				# $hsp->length('query') : length of query participating in alignment minus gaps 
	           	if(( ($hsp->length('query')*100/$query_length) >  $pcq) && ( $hsp->percent_identity >= $pci )) 					
	           	{    
					if (!$print_header)
					{
						print OUT "# ".$result->algorithm()." ".$result->algorithm_version();
						print OUT "# Query: ".$result->query_name." ".$result->query_description."\n";
						print OUT "# Database: $database_name\n";
						print OUT "# Fields: Query id, Subject id, % identity, alignment length, mismatches, gap openings, q. start, q. end, s. start, s. end, e-value, bit score\n";
						$print_header=1;
					}   
					my $hsp_coords="";
					if ($hsp->strand('hit') == 1 )
						{$hsp_coords= $hsp->start('hit')."\t".$hsp->end('hit');}
					else
						{$hsp_coords= $hsp->end('hit')."\t".$hsp->start('hit');}
					my $hit_coords="";
					if ($hsp->strand('query') == 1 )
						{$hit_coords= $hsp->start('query')."\t".$hsp->end('query');}
					else
						{$hit_coords= $hsp->end('query')."\t".$hsp->start('query');}
							
					print OUT $result->query_name."\t".$hit->name."\t".$hsp->percent_identity."\t".$hsp->length('total')."\t-\t".$hsp->gaps."\t".$hit_coords."\t".$hsp_coords."\t".$hit->significance."\t".$hit->bits."\n";
					$nb_contamination++;
				}
        		}
		}
		
	}
	if ($nb_contamination == 0) #no result we print the database
	{
			print OUT "# Database: $database_name\n";
	}
	$in->close(); 	
	close(OUT);			

}

=item Usage

        $o_param->Usage();
        Print the usage of the program

=cut

sub Usage
{
print STDERR<<END
Usage:$0
Extract hit from blast which have a least pci and pcq and write hit in m8.

[Mandatory]
        --blast_file 	filename     	blast output file
[Optionnal]
        --help                       	this message
        --pci    	int	     	pourcent of identity [90%]
	--pcq    	int	     	pourcent of query [90%]
	--output	filename	output file [inputfile.filtered]
END
}

