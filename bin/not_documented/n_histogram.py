#!/usr/local/bioinfo/bin/python2.5

from Bio import SeqIO
import sys
import string
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
from optparse import *

if __name__ == "__main__":

    parser = OptionParser(usage="Usage: n_histogram.py -i FILE [options]")
    parser.add_option("-i", "--in", dest="fasta_file",
                      help="The fasta file", metavar="FILE")
    (options, args) = parser.parse_args()
    
    if options.fasta_file == None :
        parser.print_help()
        sys.exit(1)
    else :

        x = []
        for seq_record in SeqIO.parse(open(options.fasta_file, "rU"), "fasta") :
            x.append(seq_record.seq.count("n") + seq_record.seq.count("N"))

        plt.hist(x, 50, facecolor='green', alpha=0.75)
        plt.grid(True)
        plt.savefig(options.fasta_file+".nhist.png")
        
        sys.exit(0)