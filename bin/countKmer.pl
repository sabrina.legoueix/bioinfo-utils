#! /usr/bin/perl
# $Id$

=pod

=head1 NAME

 countKmer.pl

=head1 SYNOPSIS

 countKmer.pl SAM_FILE SEARCH_FILE

=head1 DESCRIPTION

 View the number of occurrence of each k-mer.
 Counting can be:
     - to a position
       => Number of reads that contain the k-mers at the given position
     - Global with no overlap
       => Sum of the number of separate occurrences (one occurrence only covers
       not another partially) of all reads.
     - Global with overlap
       => Sum of the number of occurrences of all reads (an occurrence can
       partially overlap another).

=head1 ARGUMENTS

 1-    Fichier sam contenant les sequences ou sont recherches les K-mers.
 2-    Fichier contenant la liste des k-mers avec leurs positions.
       Format : position_dans_la_sequence<TAB ou espace>k-mer1
                ...
                position_dans_la_sequence<TAB ou espace>k-merN
       Note : pour compter le nombre total d'occurence d'un k-mer la
       position est remplacee par 'all' et par 'all_overlap' pour le compte
       avec chevauchement.

=head1 EXAMPLE

 countKmer.pl SAM_FILE SEARCH_FILE

=head1 AUTHORS

 Escudie Frederic - Plateforme genomique Toulouse (get-plage.bioinfo@genotoul.fr)

=head1 VERSION

 1.2

=head1 DATE

 21/05/2013

=head1 KEYWORDS

 kmer samfiles

=head1 DEPENDENCIES

 Aucune.

=cut


#############################################################################################################################
#
#		LIBRAIRIES
#
#############################################################################################################################
use strict ;
use warnings ;
use Getopt::Long ;
use Pod::Usage ;


#############################################################################################################################
#
#		FONCTION PRINCIPALE
#
#############################################################################################################################
MAIN :
{
	my $sam_file         = $ARGV[0] ;
	my $file_search_list = $ARGV[1] ;
	my $help             = 0 ;
	my $man				 = 0 ;

	#Gestion des paramètres
	GetOptions(
	            "help|h"        => \$help,
	            "man"			=> \$man
	)
	or pod2usage( "Try '$0 --help' for more information." );

	if( $help || $man || @ARGV != 2 )
	{
		pod2usage(
		           -sections => "SYNOPSIS|ARGUMENTS|DESCRIPTION|VERSION",
		           -verbose => 99
		) ;
	}

	#Récupérer les chaînes à rechercher
	my @searches = () ;
	loadSearches($file_search_list, \@searches) ;

	#Compter le nombre d'occurences
	open(my $SAM_FILE, $sam_file) or die "Impossible d'ouvrir le fichier ".$sam_file."\n" ;

	while( my $current_line = <$SAM_FILE> )
	{
		my %line_info = parseSamLine( $current_line ) ;

		#Pour chaque chaîne recheché
		for( my $i = 0 ; $i < @searches ; $i++)
		{
			#Si il s'agit d'une recherche globale avec overlap
			if( $searches[$i]{'position'} eq 'all_overlap' )
			{
				$searches[$i]{'count'} += findNbWithOverlap($line_info{'SEQ'}, $searches[$i]{'search'}) ;
			}
			#Si il s'agit d'une recherche globale sans overlap
			elsif( $searches[$i]{'position'} eq 'all' )
			{
				$searches[$i]{'count'} += findNb($line_info{'SEQ'}, $searches[$i]{'search'}) ;
			}
			#Sinon (il s'agit d'une recherche sur une position donnée)
			else
			{
				$searches[$i]{'count'} += findOn($line_info{'SEQ'}, $searches[$i]{'search'}, $searches[$i]{'position'}) ;
			}
		}
	}
	
	close $SAM_FILE ;

	#Afficher le comptage
	for( my $i = 0 ; $i < scalar(@searches) ; $i++)
	{
		print $searches[$i]{'search'}." on ".$searches[$i]{'position'}."\t: ".$searches[$i]{'count'}."\n" ;
	}
}


#############################################################################################################################
#
#		SOUS-FONCTIONS
#
#############################################################################################################################

=head2 procedure loadSearches

 Title        : loadSearches
 Usage        : loadSearches( $list_file, $searches_list )
 Prerequisite : none
 Function     : Remplir à partir d'un fichier la liste des k-mers.
                Le tableau aura lastructure suivante :
                [0]{'search'}=>'AAA',  [0]{'position'}=>2, [0]{'count'}=>0
                [1]{'search'}=>'TCAA', [0]{'position'}=>8, [0]{'count'}=>0
 Returns      : none
 Args         : $list_file       String    - Chemin du fichier contenant la
                                 position et la sequence des k-mers 
                                 (pos<TAB>seq).
                $searches_list   Array Ref - Reference de la liste a completer.
 Globals      : none

=cut

sub loadSearches
{
	my ($list_file, $searches_list) = @_ ;
	my $search_nb = 0 ;

	open(my $SEARCH_FILE, $list_file) or die "Impossible d'ouvrir le fichier ".$list_file."\n" ;
	
	#Pour chaque chaîne recherché
	while( my $current_search = <$SEARCH_FILE> )
	{
		chomp($current_search) ;
		
		my ($position, $string_search) = split(/\s+/, $current_search) ;
		my %search = (
		               'search'   => $string_search,
		               'position' => $position,
		               'count'    => 0
		) ;
		$$searches_list[$search_nb] = \%search ;
		$search_nb++ ;
	}

	close $SEARCH_FILE ;
}

=head2 function findOn

 Title        : findOn
 Usage        : $binary = findOn( $string, $search, $position )
 Prerequisite : none
 Function     : Retourne vrai si $search est trouve a la position $position sur
                la chaine $string.
 Returns      : none
 Args         : $string      String - Chaine sur laquelle on effectue la
                             recherche.
                $search      String - Chaine de caracteres recherchee.
                $position    Int    - Position du premier caractere de $search
                             sur la chaine $string.
 Globals      : none

=cut

sub findOn
{
	my ($string, $search, $position) = @_ ;
	my $is_ok = 0 ;

	my $regexp_pattern = '^.{'.($position-1).','.($position-1).'}'.$search ;

	#Si $string contient la chaîne recherché en position $position
	if( $string =~ /$regexp_pattern/ )
	{
		$is_ok = 1 ;
	}

	return $is_ok ;
}


=head2 function findNb

 Title        : findNb
 Usage        : $nb_occurence = findNb( $string, $search )
 Prerequisite : none
 Function     : Retourne le nombre de $search dans la chaine $string.
 Returns      : none
 Args         : $string      String - Chaine sur laquelle on effectue la
                             recherche.
                $search      String - Chaine de caracteres recherchee.
 Globals      : none

=cut

sub findNb
{
	my ($string, $search) = @_ ;

	my $regexp_pattern = '('.$search.')' ;

	my @find = $string =~ /$regexp_pattern/g ;

	return scalar(@find) ;
}


=head2 function findNbWithOverlap

 Title        : findNbWithOverlap
 Usage        : $nb_occurence = findNbWithOverlap( $string, $search )
 Prerequisite : none
 Function     : Retourne le nombre de $search dans la chaine $string. Cette
                recherche autorise les chevauchement entre les $search trouvés.
 Returns      : none
 Args         : $string      String - Chaine sur laquelle on effectue la
                             recherche.
                $search      String - Chaine de caracteres recherchee.
 Globals      : none

=cut

sub findNbWithOverlap
{
	my ($string, $search) = @_ ;
	my $nb_find = 0 ;

	my $regexp_pattern = '^'.$search ;

	#Tant que la sous-partie courante de $string est au moins aussi longue que la chaîne recherchée
	my $sub_part = $string ;
	while( length($sub_part) >= length($search)  )
	{
		#Ajouter 1 si la chaîne commence par $search
		$nb_find += $sub_part =~ /$regexp_pattern/ ;

		#Avancer d'un caractère dans string
		$sub_part = substr($sub_part, 1, length($sub_part)) ;
	}

	return $nb_find  ;
}


=head2 function parseSamLine

 Title        : parseSamLine
 Usage        : %hash = parseSamLine( $line )
 Prerequisite : none
 Function     : Returns a hash from one line of a SAM file.
 Returns      : Hash
 Args         : $line               String    - One line of a SAM file.
 Globals      : none

=cut

sub parseSamLine
{
	my ($line) = @_ ;
	my %line_info = () ;
	
	chomp($line) ;

	my @line_subdivisions = split( "\t", $line ) ;
	my $nb_fields = scalar( @line_subdivisions ) ;
	
	#Is not header
	if( !($line =~ /^@.*/) )
	{
		#Mandatory fields
		%line_info = (
			'QUERY'   => $line_subdivisions[0],
		        'FLAG'    => $line_subdivisions[1],
			'REFNAME' => $line_subdivisions[2],
			'CIGAR'   => $line_subdivisions[5],
			'SEQ'     => $line_subdivisions[9] ) ;

		#Optional fields
		for( my $i = 11 ; $i < $nb_fields ; $i++ )
		{
			if( $line_subdivisions[$i] =~ /([^\:]+)\:[^\:]+\:([^\s]*)/ )
			{
				$line_info{$1} = $2 ;
			}
		}
	}

	return %line_info ;
}


#############################################################################################################################
#
#		A faire
#
#############################################################################################################################

#retourne :
#1 si read 1
#2 si read 2
#single si single read
sub readMate
{
	my ($sam_line_obj) = @_ ;
#Si notation -X
#$sam_line_obj{'FLAG'} =~ /(\d)$/
#Si notation num
}

