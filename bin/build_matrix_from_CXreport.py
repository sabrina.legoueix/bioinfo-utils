#!/usr/local/bioinfo/bin/python2.5

import sys
import os
from optparse import *
from itertools import imap

__name__ = "build_matrix_from_CXreport.py"
__synopsis__ = "build_matrix_from_CXreport.py -i CX_report.txt --context [CG|CHG|CHH]"
__date__ = "2013/07"
__authors__ = "Celine Noirot"
__keywords__ = "Bisulfite Bismark Matrix "
__description__ = "This script build a matrix at each position of a context if the reverse strand is also methylated"
__version__ = '1.0'


def pearsonr(x, y):
  # Assume len(x) == len(y)
  n = len(x)
  sum_x = float(sum(x))
  sum_y = float(sum(y))
  sum_x_sq = sum(map(lambda x: pow(x, 2), x))
  sum_y_sq = sum(map(lambda x: pow(x, 2), y))
  psum = sum(imap(lambda x, y: x * y, x, y))
  num = psum - (sum_x * sum_y/n)
  den = pow((sum_x_sq - pow(sum_x, 2) / n) * (sum_y_sq - pow(sum_y, 2) / n), 0.5)
  if den == 0: return 0
  return num / den

parser = OptionParser(usage="Usage: build_matrix_from_CXreport.py -i FILE -c STRING")
parser.add_option("-i", "--input", dest="cx_report",
                  help="The CX_report.txt file outputs from bismark_methylation_extractor with option --CX-context", metavar="FILE")
parser.add_option("-c", "--context", dest="context",
                  help="Specify the context to study", metavar="FILE")
parser.add_option("-o", "--output", dest="output",
                  help="Output", metavar="FILE")
(options, args) = parser.parse_args()


if options.cx_report == None or not os.path.exists(options.cx_report) or options.context == None or options.output == None:
    parser.print_help()
    sys.exit(1)
 
f=open (options.cx_report, "r")
fout=open (options.output, "w")
fout.write("Compute matrix of Cm / Total C at position n and n+1\n if is context : "+options.context + "\n and if n and n+1 are not in same strand\n" )
fout.write("Pos\tCm/All_C at n\tCm/All_C at n+1\tstrand at n\n")
N_values_forward=[]
N_values_reverse=[]
 
 
for line in f :
    #<chromosome>  <position>  <strand>  <count methylated>  <count non-methylated>  <C-context>  <trinucleotide context>
    line=line.strip()
    if line == "" : continue 
    (chr,pos,strand,c_met,c_unmet,c_context,tri_nuc_context)=line.split('\t')
    if c_context == options.context and int(c_met)>0:
        if previous_strand != strand and int(previous_pos)+1 == int(pos) and int(previous_c_met)>0:
            C_previous_m=float(float(previous_c_met)/(float(previous_c_met)+float(previous_c_unmet))*100)
            C_m=float(float(c_met)/(float(c_met)+float(c_unmet))*100)
            if previous_strand != strand :
                N_values_forward.append(C_previous_m)
                N_values_reverse.append(C_m)

            fout.write( "\t".join([previous_pos,str(C_previous_m),str(C_m),previous_strand])+"\n" )
            
    (previous_chr,previous_pos,previous_strand,previous_c_met,previous_c_unmet,previous_c_context)=(chr,pos,strand,c_met,c_unmet,c_context)

if previous_strand == "+" :
    N_values_forward.append(C_previous_m)
else :
    N_values_reverse.append(C_previous_m)    

print "Correlation on forward VS reverse values : " + str(pearsonr(N_values_forward,N_values_reverse))
fout.close()
