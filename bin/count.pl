#!/usr/bin/perl -w

use strict;
use Getopt::Long;
use Pod::Usage;


# Options definitions (name,format,default,required)
my @Options = ( ['help',undef,undef,0],
		['man',undef,undef,0],
	      );
# defaults values
my %Values=();
my %Required=();
map {$Values{$_->[0]}=$_->[2];$Required{$_->[0]}=$_->[3];} @Options;
# build options list
my %Options=();
map {$Options{defined($_->[1])?$_->[0].'='.$_->[1]:$_->[0]}=\$Values{$_->[0]};} @Options;
# retrieve options
GetOptions(%Options) || pod2usage(2);
pod2usage(1) if ($Values{'help'});
pod2usage(-exitstatus => 0, -verbose => 2) if ($Values{'man'});

# check required
map {pod2usage("$_ is required") if ($Required{$_}&&!defined($Values{$_}));} keys %Values;


my $line;
my %C;

while ($line=<STDIN>)
{
	chomp $line;
	my @T = split /\s+/,$line;
	# generate key
	my @K = ();
	map {push @K, $T[$_ - 1]} @ARGV;
	my $k = join(' ', @K);
	# count
	if (exists($C{$k})) { $C{$k}++; }
	else { $C{$k} = 1; }
}
map {print "$_ $C{$_}\n";} keys %C;

#============================================================

=head1 NAME

count.pl

=head1 SYNOPSIS

count.pl [--help|--man] [col# *]

=head1 DESCRIPTION

Count occurrences of multi columns keys in a blank characters delimited columns text file
Reads as STDIN a blank characters delimited columns text file, use columns numbers specifieds by remaining parameters to build a multi columns key, 
and counts occurrences of text lines having this key.
Writes as STDOUT for each key : key nb_occur, all column are delimited by a space

=head1 OPTIONS

=over 8

=item B<-help>

Print a brief help message and exits.

=item B<-man>

Prints the manual page and exits.

=back

=head1 AUTHORS

Patrice DEHAIS

=head1 CONTACT

Questions can be posted to the sigenae mailing list:
sigenaesupport@jouy.inra.fr

=cut
