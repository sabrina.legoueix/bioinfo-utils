#!/bin/env python

from Bio import SeqIO
import sys
import string
import argparse

__name__ = "fasta_extract_length.py"
__synopsis__ = "python clean_fasta.py -f file_name.fasta -m minimum_lenght -M maximum_length > STDOUT"
__date__ = "2012/10"
__authors__ = "Christophe Klopp"
__keywords__ = "fasta line lenght"
__description__ = "This script extracts sequences from a fasta file using minimum and maximum sequence length."
__version__ = '1.0'

parser=argparse.ArgumentParser(description=__description__)
parser.add_argument('-f', action='store', dest='fasta_file',  help='name of the fasta file', required=True)
parser.add_argument('-m', action='store', dest='min_lenght', type=int, default=100, help='minimum lenght of the sequences [100]')
parser.add_argument('-M', action='store', dest='max_lenght', type=int, default=10000, help='maximum lenght of the sequences [10000]')
parser.add_argument('--version', action='version', version='%(prog)s 0.0')
args=parser.parse_args()

for seq_record in SeqIO.parse(open(args.fasta_file), "fasta") :
	if len(seq_record) >= args.min_lenght and len(seq_record) <= args.max_lenght :
		print ">"+seq_record.id
		print seq_record.seq


