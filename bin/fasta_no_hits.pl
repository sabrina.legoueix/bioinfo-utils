#!/usr/bin/perl

use strict;
use warnings ;
use Getopt::Long;
use Bio::SearchIO;
use Bio::SeqIO;
use Carp;
use Bio::Seq;


=pod

=head1 NAME

fasta_no_hits.pl

=head1 DESCRIPTION

Take as input file a blast results file and a fasta file. The script output fasta sequences of no hit queries.
 
=head1 SYNOPSIS

fasta_no_hits.pl -blast file.blast -fasta file.fasta > output.fasta

=head1 AUTHORS

Barrilliot K.

=head1 VERSION 

1

=head1 DATE

01/2013

=head1 KEYWORDS

blast fasta hits



 

=cut

my ($noblast,$fasta,$ligne1,$ligne2,@no_hit,$taille,$description,@cols,$val,$cuff);

GetOptions(	"blast:s" => \$blast,
		"fasta:s" => \$fasta);

my $searchio = Bio::SearchIO->new( -format => 'blast', -file   => $blast);
while ( my $result = $searchio->next_result() ) {
  my $nom_seq = $result->query_name;
  if ($result->num_hits == 0) {
    push (@no_hit,$result->query_name );
  }
}
$taille=@no_hit;

my $in = Bio::SeqIO-> new(
      -file     => $fasta ,
      -format => 'Fasta'
);

while ( my $seq = $in->next_seq() ) {
		my $id = $seq->display_name;
		my $sequence = $seq->seq();
		for (my $i=1;$i<$taille;$i++){
			if ($id eq $no_hit[$i]){
				print ">";print "$id\n"; print "$sequence\n";
  			}else { $i++;}
		}
 
}
