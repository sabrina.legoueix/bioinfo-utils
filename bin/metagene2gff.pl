#!/usr/bin/perl
 
# file: generate_gff_blastm8_metagene.pl
# This is code generates single output files for multiple entries blast and metagene files 
# Author: Christophe Klopp
# the arguments given represent the different files which will be parsed in order to generate the gff output file
# the name of the file is 'database'.'program' example swissprot.blast the file is processed depending on the 
# name of the program which has generated the file
#
 
use strict;
use File::Basename;
use Switch;

use constant USAGE =><<END;
Usage: $0 <file>
   Render a metagene result file into GFF form.

Example to try:
   metagene2gff.pl file_result_from_metagene
 
END


sub parse_metagene{
	my ($file)=shift;
	#print $file;
	my $display_name;
	my $indic;
	my $ind_meta = 1; #metagene feature index 
	my $res = '';
	open(HANDLE, $file);
	foreach my $line (<HANDLE>){ # read blast file	
  		#print $line;
  		chomp($line);
  		if (($line =~ /^\#/) && ($indic > 0)){
			#print "cas 1\n";
			next;
  		}  # ignore comments
  		if ($line =~ /^$/){
			#print "cas 2\n";
			$indic = 0;
			next;
  		}  # ignore comments
  		my @elements =  split(/\t/,$line);
  		if ($line =~ /^\#/){
			#print "cas 3 ".(length($elements[0])-1)."\n";
			$display_name = substr($elements[0],2, length($elements[0])-1);
			#$display_name =$elements[1];
			$indic = 1;
			next;
  		}
  		#print $display_name."\n";
 
  		#my($name,$score,$start,$end) = split /\t+/;
  		my $strand = "+";
  		if ($elements[2] eq '-'){
			$strand = "-";
  		}
  		$res .= $display_name."\tmetagene$ind_meta\tmetagene\tdetection\t".$elements[0]."\t".$elements[1]."\t100\t".$strand."\t\.\t".$elements[4]."\t".$elements[5]."\n";
		$ind_meta++;
	}
	close(HANDLE);
	return $res;
}


my $handle;
if (!$ARGV[0]){
	print "missing input file!";
	print USAGE;
	exit;
}

# creating the output files for all the features 
my $arg ='';

my $res = &parse_metagene($ARGV[0]);
#print $res;
foreach my $line (split(/\n/,$res)){
	my @blocks = split(/\t/,$line);
	print join("\t", @blocks[1..8]);
	print "\n";
}



=pod

=head1 NAME

metagene2gff.pl

=head1 SYNOPSIS

metagene2gff.pl file_result_from_metagene > STDOUT

=head1 DESCRIPTION

Convert a metagene output file to a gff file

=head1 AUTHORS

Celine Noirot

=head1 VERSION

1

=head1 DATE

06/2012

=head1 KEYWORDS

metagene gff conversion

=cut


