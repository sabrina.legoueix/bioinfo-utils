#!/usr/bin/env perl

#
#       vcf_make_stats_by_group.pl
#
#       Copyright 2012 Sylvain Marthey <sylvain.marthey@jouy.inra.fr>
#       
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, see <http://www.gnu.org/licenses/>.


use strict;
use Getopt::Long;
use Pod::Usage;
use Vcf;
use Getopt::Long;
use Pod::Usage;
use Data::Dumper;

my( $help, $man, $groups_file, $print_header,$GQ);

# on recupere les options
GetOptions("help|?" => \$help,
	   "man"    =>  \$man,
           "groups_file:s" => \$groups_file,
           "print_header:s" => \$print_header,
           "GQ:s" => \$GQ,
           )
or pod2usage( "Try '$0 --help' for more information.");

pod2usage(-exitval =>1, -verbose => 2) if ($help);

pod2usage( -verbose => 1 ) if $help;
pod2usage( -verbose => 2 ) if $man;

$GQ ||= 0;

if ($groups_file eq "" || !-f($groups_file))
  {
     pod2usage(-message=>"\n--groups_file parameter missing or is not a valid file path.\nTry `$0 --help' for more information.\n",
              -exitval => 2,-verbose=> 0);
  }

my %groups = parse_groups('file' => $groups_file);
make_stats_for_groups('groups' => \%groups);

#--------------------------------



sub parse_groups
{
	my %args = @_;
	my $file = $args{file};
	
	my %groups;
	my $i=1;
	
	open(IN, "$file") or die("Can not open $file: $!");
	while(<IN>){
		chomp;
		next if(!$_);
		my @line = split(/\t/,$_);
		if(!$line[1] || !$line[0]){
			die "error in group file line $i\n";
		}

		push(@{$groups{$line[1]}},$line[0]);
		$i++;
	}
	close(IN);
	return %groups;
}


sub make_stats_for_groups
{
    my %args = @_;
    my %groups = %{$args{groups}};
    my %sample; 
    my $vcf = Vcf->new(fh=>\*STDIN);
    $vcf->parse_header();
    my @grp_order = sort(keys(%groups));
    
    # sample identification
    foreach my $k (@grp_order){
	foreach my $sample (@{$groups{$k}}){
		if(!$$vcf{has_column}{$sample}){
			print "sample ".$sample." not found in VCF file\n";
		}
	}
    }
    
    my %infos_col;
    my $header;

    # create header line
    for (my $i = 0; $i< scalar(@{$$vcf{columns}});$i++){
	last if($$vcf{columns}[$i] ne "CHROM" && $$vcf{columns}[$i] ne "POS" && $$vcf{columns}[$i] ne "ID" && $$vcf{columns}[$i] ne "REF" && $$vcf{columns}[$i] ne "ALT" && $$vcf{columns}[$i] ne "QUAL" && $$vcf{columns}[$i] ne "FILTER" && $$vcf{columns}[$i] ne "INFO" && $$vcf{columns}[$i] ne "FORMAT" && $$vcf{columns}[$i]);
	if($$vcf{columns}[$i] ne "FILTER" && $$vcf{columns}[$i] ne "INFO"){
		$infos_col{$$vcf{columns}[$i]}++;
		$header .= $$vcf{columns}[$i]."\t";
	}
    }
    foreach my $k (@grp_order){
	$header .= "[$k] Fq Allele Ref\t[$k] Fq Allele Alt1\t[$k] Nb Reads Ref\t[$k] Nb Reads Alt1\t[$k] Best GT\t[$k] Nb Sample Best GT";
	foreach my $sample (@{$groups{$k}}){
		$header .= "\t$sample";
	}
	$header .= "\t";
    }

    if(uc($print_header) eq "YES" || uc($print_header) eq "Y" || $print_header eq "" ){
	print $header."\n";
    }
    # print datata lines
    while (my $x=$vcf->next_data_hash())
    {
	my $line;

	# print context information
	my $infos;
	$infos.= $$x{CHROM}."\t".$$x{POS}."\t".$$x{ID}."\t".$$x{REF}."\t";
	foreach my $alt (@{$$x{ALT}}){
		$infos.= $alt.",";
	}
	$infos =~ s/,$//;
	
	$infos.= "\t".$$x{QUAL}."\t";
		
	foreach my $el (@{$$x{FORMAT}}){
		$infos.= $el.":";
	}
	$infos =~ s/:$//;	
	
	foreach my $k (@grp_order){
		my %genotypes;
		my @alls = (0,0);
		my @nb_reads = (0,0);
		my $temp_string;
		# calc stats for the group
		foreach my $sample (@{$groups{$k}}){
			if($$x{gtypes}{$sample}{GT} ne './.'){
				my @temp = split(/\//,$$x{gtypes}{$sample}{GT});
				if($$x{gtypes}{$sample}{GQ} > $GQ){
					# store likelihood for this genotype 
					push(@{$genotypes{$$x{gtypes}{$sample}{GT}}},$$x{gtypes}{$sample}{GQ});
					$alls[$temp[0]]++;
					$alls[$temp[1]]++;
				}
				$temp_string .= "\t";
				my @temp2 = split(/\,/,$$x{gtypes}{$sample}{AD});
				$nb_reads[$temp[0]]+= $temp2[0];
				$nb_reads[$temp[1]]+= $temp2[1];
				foreach my $f (@{$$x{FORMAT}}){
					$temp_string .= $$x{gtypes}{$sample}{$f}.":";
				}
				$temp_string =~ s/:$//;
			}else{
				$temp_string .= "\t./.";
			}
		}

		# print group stats
		if(($alls[0]+$alls[1])>0){
			$line .= "\t";
			$line .= sprintf("%.3f", ($alls[0]/($alls[0]+$alls[1])));
			$line .= "\t";
			$line .= sprintf("%.3f", ($alls[1]/($alls[0]+$alls[1])));
			$line .= "\t".$nb_reads[0]."\t".$nb_reads[1];
			$line .= "\t";
			# search the genotype with the best cumulative likelihood
			my @best_lkhd;
			my @best_gt;
			my @nb_sample;
			foreach my $gt (keys(%genotypes)){
				my $sum;
				foreach my $gq (@{$genotypes{$gt}}){
					$sum += $gq;
				}
				if(!defined @best_lkhd || $best_lkhd[0] < $sum){
					@best_lkhd = ($sum);
					@nb_sample = (scalar(@{$genotypes{$gt}}));
					@best_gt = ($gt);
				}elsif($best_lkhd[0] == $sum){
					push(@best_lkhd,$sum);
					push(@nb_sample,scalar(@{$genotypes{$gt}}));
					push(@best_gt,$gt);
				}
			}
			my $gts;
			my $samples;
			for (my $i=0;$i < scalar(@best_gt); $i++){
				$gts .= $best_gt[$i]." ";
				$samples .= $nb_sample[$i]." ";
			}
			$gts =~ s/ $//;
			$samples =~ s/ $//;
			$line .= $gts."\t".$samples.$temp_string;
		}elsif($nb_reads[0]+$nb_reads[1]>0){
			$line .= "\t.\t.\t".$nb_reads[0]."\t".$nb_reads[1]."\t.\t.$temp_string";
		}else{
			$line .= "\t.\t.\t.\t.\t.\t.$temp_string";
		}
	}
	print $infos.$line."\n";
    }
}


=pod

=head1 NAME

vcf_make_stats_by_group.pl

=head1 SYNOPSIS

vcf_make_stats_by_group.pl [options] < in.vcf > out.tab

=head1 DESCRIPTION

vcf_make_stats_by_group.pl reads the input VCF file and for each SNP position calculates the ref and alt allelic frequencies, count the number of reads counts the number of reads supporting each allele and predict the best genotype of each individual group. 

Options:
           -h, -?, -help, -man              This help message.
           -groups_file                     Text file containing groups
                                            format: sample_id\tgroup_name\n.
           -print_header                    [yes|no]
           -GQ                              Genotype quality thresold. 

Outputs : Tab separated file containing for each groups the folowing fields :

      Fq Allele Ref => nb of ref allele in GT predictions / nb total of predicted alleles in GT fields 
      
      Fq Allele Alt1 => nb of Alt1 allele in GT predictions / nb total of predicted alleles in GT fields
      
      Nb Reads Ref => sum of all reads supporting Ref allele in AD fields
      
      Nb Reads Alt1 => sum of all reads supporting Alt1 allele in AD fields
      
      Best GT => genotype with the best cumulative likelihood in all sample
      
      Nb Sample Best GT => number of sample sharing the Best GT  
      
           
!!! Warning !!! Work only for vcf files produced by GATK 
           
=head1 AUTHORS

Sylvain Marthey <sylvain.marthey@jouy.inra.fr>

=head1 VERSION

1.2

1.1 => 1.2 (Update documentation)


=head1 DATE

2012

=head1 KEYWORDS

vcf, allelic frequencie, GATK

=head1 EXAMPLE

vcf_make_stats_by_group.pl -groups_file my_groups.tsv < SNPs_chr13.vcf > SNPs_chr13.tsv

=cut
