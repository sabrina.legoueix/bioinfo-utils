#
# Copyright (C) 2009 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from optparse import *
import sys
from os.path import join,splitext, basename, exists, split


__name__ = "htseqcount_merge.py"
__synopsis__ = "htseqcount_merge.py -f fof -o output -c"
__date__ = "05/2013"
__authors__ = "Celine Noirot"
__keywords__ = "htseq-count merge expression"
__description__ = "htseqcount_merge.py merge all files listed in fof file."
"""--fof : fof format requires one column with filepath, and an optionnal one with library name.
-c : Do not print htseq count values : no_feature, ambiguous, too_low_aQual, not_aligned, alignment_not_unique.
--output : output file. 
"""
__version__ = '1.0'
__copyright__ = 'Copyright (C) 2009 INRA'
__license__ = 'GNU General Public License'
__status__ = 'beta'
__email__ = 'support.genopole@toulouse.inra.fr'


def read_count_file (file,libname,dict_count):
    """
    Extract count from the count file 
      @param file          : the input count file (output of sam2count)
      @param libname       : the library name   
      @param dict_count    : the dict of contigs to update
    """
    fin=open (file, "r")
    
    # parse headers
    line = fin.readline()
    
    #parse data
    line = fin.readline()
    while line:
        
        if line.startswith("#") : continue
        tab = line.rstrip().split("\t") # contig count line
        if len(tab) > 0 :
            if not dict_count.has_key(tab[0]) : # if new contig
                dict_count[tab[0]] = {}
            if not dict_count[tab[0]].has_key(libname) : # if new library for this contig
                dict_count[tab[0]][libname] = tab[1]
        line = fin.readline()
    fin.close

def version_string ():
    """
    Return the htseqcount_merge version
    """
    return "htseqcount_merge.py " + __version__


    
parser = OptionParser(usage="Usage: %prog", description = "Merge htseq-count output file into a global counting file")

igroup = OptionGroup(parser, "Input options","")
igroup.add_option("-f", "--fof", dest="fof", help="file of count file\n requires one column with filepath, and an optionnal one with library name")
igroup.add_option("-c", "--feature-only", dest="feature_only", help="Do not print htseq count values : no_feature, ambiguous, too_low_aQual, not_aligned, alignment_not_unique",action="store_true",default=False)
parser.add_option_group(igroup)

ogroup = OptionGroup(parser, "Output files options","")
ogroup.add_option("-o", "--output", dest="output", help="the output count file")
parser.add_option_group(ogroup)

(options, args) = parser.parse_args()

if options.fof == None :
    sys.stderr.write("Need a file of file\n")
    parser.print_help()
    sys.exit(1)

if options.output == None:
    sys.stderr.write("Output file is missing\n")
    parser.print_help()
    sys.exit(1)

dict_count={}

#parcours des fichiers 
if options.fof != None :
    fin=open (options.fof, "r")
    line=fin.readline()
    while line != "":
        file = line.rstrip()
        tab = line.split("\t")
        if len(tab) > 1 : file = tab[0]
        if exists(file) :
            current_libname = splitext(basename(file))[0]
            if len(tab) > 1 : current_libname = tab[1]
            read_count_file(file,current_libname,dict_count)
        else :
            sys.stderr.write("File : " + file + " doesn't exist")
            sys.exit(1)
        line = fin.readline()
    fin.close

    fout = open (options.output,"w")
    header_printed=0
    header="ref\t"
    final_values=["no_feature","ambiguous","too_low_aQual","not_aligned","alignment_not_unique"]
    for contig_name in sorted(dict_count.keys()) :
        line = contig_name+"\t"
        if any(contig_name in item for item in final_values) : continue
        for lib in sorted(dict_count[contig_name].keys()) :
            if ( header_printed == 0):
                header+=lib+ "\t"
            line += str(dict_count[contig_name][lib]) + "\t"
            
        if ( header_printed == 0):
            fout.write (header+"\n")
            header_printed=1
        fout.write (line+"\n")
    
    #finals values
    if (not options.feature_only) :
        for contig_name in final_values :
            line = contig_name+"\t"
            for lib in sorted(dict_count[contig_name].keys()) :
                 line += str(dict_count[contig_name][lib]) + "\t"
            fout.write (line+"\n")
        fout.close
sys.exit(0)
        