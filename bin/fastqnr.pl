#!/usr/bin/perl -w
use strict;
use Digest::MD5 qw(md5);

# read a fastq and compute for each sequence its md5 (16 bytes)
# skip the sequence if the md5 has been already seen
# otherwize print the fastq entry
# 1000 non redundant sequences will require at least 16Kb
# 1e6 => 16Mb
# 1e9 => 16Gb

my %MD5=();
while (my $line=<STDIN>) {
  my $ac=$line;
  my $seq=<STDIN>;
  my $ac2=<STDIN>;
  my $qual=<STDIN>;
  my $md5=md5($seq);
  next if (exists($MD5{$md5}));
  $MD5{$md5}=undef;
  printf "%s%s%s%s",$ac,$seq,$ac2,$qual;
}

=pod

=head1 NAME

fastqnr.pl

=head1 SYNOPSIS

fastqnr.pl < IN.fastq > OUT.fastq

=head1 DESCRIPTION

Read a fastq and compute for each sequence its md5 (16 bytes)
skip the sequence if the md5 has been already seen  otherwize
print the fastq entry.
# 1000 non redundant sequences will require at least 16Kb
# 1e6 => 16Mb
# 1e9 => 16Gb

=head1 AUTHORS

Patrice Dehais

=head1 VERSION

1

=head1 DATE

2013

=head1 KEYWORDS

Fastq Redundancy 

=head1 EXAMPLE

fastqnr.pl < IN.fastq > OUT.fastq

=cut


