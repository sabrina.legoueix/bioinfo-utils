#!/usr/bin/perl -w

=pod

=head1 NAME

	bam2stat.pl
	
=head1 SYNOPSIS

	bam2stat.pl  -b file.bam [-s <BWA_aln|BWA_bwasw> ]

=head1 DESCRIPTION

	Calculate BWA BAM statistics: number of read paired/single uniquely/multi/not intra/inter sequence mapped:

=head1 OPTIONS

=over

=item B<-b>

	bam file of paired reads!

=item B<-s>
	
	bwa algorithm software : BWA_aln or BWA_bwasw (some SAM TAG are not used with the bwasw algorithm, like X0:i)
	
=back

=head1 AUTHORS
	
	Maria Bernard - mabernad@jouy.inra.fr 

=head1 VERSION

    1

=head1 DATE

	# created on: 2012/06/07
	# modified by:
	# modified on: 
    
=head1 KEYWORDS

    Alignment BAM

=head1 EXAMPLE

	bam2stat  -b file.bam

=cut

use strict;
use Getopt::Long;
use Pod::Usage;

our (  $SOFT, $BAM, $HELP ) 
    = (  "BWA_aln" , "",  0 ); 
my $result = &GetOptions (	"s=s"	=>\$SOFT,
				"b=s"	=>\$BAM,
  				"h"		=> \$HELP );
pod2usage(1) if ($BAM eq "" || $HELP);
if (! -e $BAM){ error("$BAM does not exists");}
if ($SOFT ne "BWA_aln" && $SOFT ne "BWA_bwasw" ){ error("You must choose between BWA_aln or BWA_bwasw");}

# Run bam2stat
my %stat=(	"PE uniq intra"=>0,
		"PE uniq trans"=>0,
		"SE uniq (mate unmapped)"=>0,
		"SE multiple (mate unmapped)"=>0,
		"PE multiple"=>0,
		"PE unmapped"=>0,
		"PE uniq multiple"=>0,
		"other"=>0,
		"total"=>0);
		
if ($SOFT eq "BWA_aln") { bam2statALN($BAM,\%stat);}
else {bam2statSW($BAM,\%stat);}

# OUTPUT 

foreach my $key ("PE uniq intra","PE uniq trans","SE uniq (mate unmapped)","PE multiple","SE multiple (mate unmapped)","PE unmapped","PE uniq multiple","other","total"){
	printf("%s\t%d\t(%.2f %%)\n",  $key, $stat{$key}, ($stat{$key}*100)/$stat{"total"} );
}


#--------------------------------------------------------#
#					FUNCTIONS							 #
#--------------------------------------------------------#
# test flag
#~ f&1==1		==> paired
#~ f&2==2		==> properly paired
#~ f&4==4		==> read unmaped
#~ f&8==8		==> mate unmaped
#~ f&16==16		==> read reverse
#~ f&32==32		==> mate reverse
#~ f&64==64		==> first in paire
#~ f&128==128	==> second in paire
#~ f&256==256	==> the alignment is not primary
#~ f&512==512	==> QC failure
#~ f&1024==1024	==> optical or PCR duplicate

#
#		bam2statALN
#--------------------------------------------------------
# take a BAM file produced by BWA aln
# return a statistics containing hash
sub bam2statALN{
	my ($IN,$stat) = @_;
	my %uniqMapped;
	my %multiMapped;
	my %unMapped;
	
	open (IN, "samtools view $BAM |") || die "Cannot read $BAM, $BAM has to be a BAM file\n";
	#~ open (IN, "cat $BAM |") || die "Cannot read $BAM, $BAM has to be a BAM file\n";
	while (<IN>){
		$stat->{"total"}++;
		chomp;
		my @data = split '\t', $_;
		my $read = $data[0];
		$read=~s/_2$// if ($read=~/_2$/);
		$read=~s/_1$// if ($read=~/_1$/);
		$read=~s/_2.fq// if ($read=~/_2.fq/);
		$read=~s/_1.fq// if ($read=~/_1.fq/);
		my $flag = $data[1];
		my $seq = $data[2];
		
		# read mapped 
		if ( ($flag&4)!=4 ){			
			# uniquely Mapped et mate aligned thanks to S&W
			if ($_ =~/X0:i:1\t/ || $_ =~/XT:A:M/){
				push @{ $uniqMapped{$read} }, $seq ;
			}else{	# cas des multimatch
				push @{ $multiMapped{$read} }, $seq;
			}
		# read unmapped
		}else{
			push @{$unMapped{$read}}, $seq;
		}		
	}
	close (IN);
	
	# parcours des hash et ecriture des stat
	foreach my $readID (keys %uniqMapped ) {
		my @seq = @{ $uniqMapped{$readID} };
		if (scalar @seq ==2){
			if ($seq[0] eq $seq[1]){
				$stat->{"PE uniq intra"}+=2;	
			}else{
				$stat->{"PE uniq trans"}+=2;	
			}
		}else{
			if (defined $unMapped{$readID}) {
				$stat->{"SE uniq (mate unmapped)"}+=2;	
			}elsif (defined $multiMapped{$readID}){
				$stat->{"PE uniq multiple"}+=2;
			}
		}
	}
	
	foreach my $readID (keys %multiMapped ) {
		my @seq = @{ $multiMapped{$readID} };
		if (scalar @seq ==2){
			$stat->{"PE multiple"}+=2;	
		}else{
			if (defined $unMapped{$readID}) {
				$stat->{"SE multiple (mate unmapped)"}+=2;	
			}
		}
	}
	
	foreach my $readID (keys %unMapped ) {
		my @seq = @{ $unMapped{$readID} };
		if (scalar @seq ==2){
			$stat->{"PE unmapped"}+=2;	
		}elsif (!defined $multiMapped{$readID} && ! defined $uniqMapped{$readID} ){
			$stat->{"other"}++;	
		}
	}
	
	return (%stat);
}

#
#		bam2statSW
#--------------------------------------------------------
# take a BAM file produced by BWA bwasw
# return a statistics containing hash
sub bam2statSW{
	my ($IN,$stat) = @_;
	my %read1Mapped;
	my %read2Mapped;
	my %unMapped;
	my %readID;
	
	
	open (IN, "samtools view $BAM |") || die "Cannot read $BAM, $BAM has to be a BAM file\n";
	while (<IN>){
		chomp;
		my @data = split '\t', $_;
		my $read = $data[0];
		$read=~s/_2$// if ($read=~/_2$/);
		$read=~s/_1$// if ($read=~/_1$/);
		$read=~s/_2.fq// if ($read=~/_2.fq/);
		$read=~s/_1.fq// if ($read=~/_1.fq/);
		if ( !defined $readID{$read}) { 
			$readID{$read}="" ;
			$stat->{"total"}+=2;
		}
		my $flag = $data[1];
		my $seq = $data[2];
		my $readSeq = $data[9];

		# read mapped 
		if ( ($flag&4)!=4 ){
			my $numRead = "1";
			$numRead = "2" if (($flag&128)==128);
			
			if ($numRead eq "1"){
				push @{ $read1Mapped{$read} }, $seq ;
				#~ print scalar @{ $uniqMapped{$read} },"\n";
			}else{	
				push @{ $read2Mapped{$read} }, $seq;
				#~ print scalar @{ $multiMapped{$read} },"\n";
			}
		# read unmapped
		}else{
			if ( !defined $unMapped{$read} ){
  				push @{$unMapped{$read}}, $readSeq;
			}else{
				my %alredy_seen = map { $_ => 1 } @{$unMapped{$read}}; 
				if( ! exists($alredy_seen{$readSeq})) {
					push @{$unMapped{$read}}, $readSeq;
				} 
			}
		}		
	}
	close (IN);
	
	# parcours des alignements et compte par catégorie uniq/mutltiple trans paire ...
	foreach my $r ( keys %readID) {
		my @seq;
		my $seq1="";
		my $scal1=0;
		if (defined $read1Mapped{$r} ){
			@seq = @{$read1Mapped{$r}};
			$scal1 = scalar @seq;
			$seq1= $seq[0] if ($scal1 == 1);
		}
		my $seq2="";
		my $scal2=0;
		if (defined $read2Mapped{$r} ){
			@seq = @{$read2Mapped{$r}} ;
			$scal2 = scalar @seq;
			$seq2 = $seq[0] if ($scal2 == 1);
		}
		
		
		if ($seq1 ne "" && $seq2 ne ""){
			if ($seq1 eq $seq2){
				$stat->{"PE uniq intra"}+=2;	
			}else{
				$stat->{"PE uniq trans"}+=2;	
			}
		}elsif ($scal1 > 1 && $scal2 > 1){
			$stat->{"PE multiple"}+=2;
		}elsif (defined $unMapped{$r} && scalar @{$unMapped{$r}} == 2){
			$stat->{"PE unmapped"}+=2;
		}elsif (($scal1==1 && $scal2 == 0 && defined $unMapped{$r} && scalar @{$unMapped{$r}} == 1) || ($scal2==1 && $scal1 == 0 && defined $unMapped{$r} && scalar @{$unMapped{$r}} == 1)){
			$stat->{"SE uniq (mate unmapped)"} += 2;
		}elsif (($scal1>1 && $scal2 == 0 && defined $unMapped{$r} && scalar @{$unMapped{$r}} == 1) || ($scal2>1 && $scal1 == 0 && defined $unMapped{$r} && scalar @{$unMapped{$r}} == 1)){
			$stat->{"SE multiple (mate unmapped)"} += 2;
		}elsif (($scal1>1 && $scal2 == 1 && ! defined $unMapped{$r}) || ($scal2>1 && $scal1 == 1 && ! defined $unMapped{$r} )){
			$stat->{"PE uniq multiple"}+=2;
		}else{
			#~ print "$r (paire with read mapped and unmapped !! \n";
			$stat->{"other"}+=2;	
		}
	}
	
	return (%stat);
}

#
#		error
#_______________________________________________________________
sub error {
	my $msg = shift;
	if(defined $msg) { warn "$msg\n"; pod2usage(1); }
	exit 1 ;
}
