#!/usr/bin/perl
use strict;
use warnings;
use Getopt::Long;
use Pod::Usage;

my %options;

Getopt::Long::Configure("no_ignorecase");
Getopt::Long::Configure("bundling");
GetOptions(	\%options,
	'fasta=s',     # fasta file
	'fastq=s',     # fastq file
	'output',      # write sequences ?
	'help|h',      # help
        ) or pod2usage(-verbose => 1);

pod2usage(-verbose => 1,-noperldoc => 1) if (exists $options{'help'});
pod2usage(-message => "A fasta or fastq file must be provided !",-verbose => 1) if (! exists $options{'fasta'} && ! exists $options{'fastq'});

# Main
my %h;

if (exists $options{'fasta'}){
    open FASTA,$options{'fasta'};
    while(<FASTA>){
        next if (/^>/);
        chomp;
        if (exists $h{$_}){
            $h{$_}++;
        }
        else{
            $h{$_}=1;
        }
    }
    close FASTA;
}

if (exists $options{'fastq'}){
    open FASTQ,$options{'fastq'};
    while(<FASTQ>){
        chomp;
        next if not ($_ =~ /^[actgnACTGN]+$/);
        if (exists $h{$_}){
            $h{$_}++;
        }
        else{
            $h{$_}=1;
        }
    }
    close FASTQ;
}



if (exists $options{'output'}){
    foreach my $k (keys %h){
        print $k,"\t",$h{$k},"\n";
    }
}
else{
    my $nbr = keys (%h);
    print $nbr;
}

=pod

=head1 NAME

count_unique_seq.pl

=head1 SYNOPSIS

count_unique_seq.pl [--fasta FILE] [--fastq FILE] [--output] [--help]

=head1 DESCRIPTION

count_unique_seq.pl counts the number of unique sequences (remove redundancy)
outputs sequences and their occurences if --output

=head1 AUTHORS

Olivier Rue

=head1 VERSION

1

=head1 DATE

05/2013

=head1 KEYWORDS

count sequences unique fasta fastq

=head1 EXAMPLE

perl count_unique_seq.pl --fasta myfile.fa

=cut

