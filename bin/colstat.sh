#!/bin/sh

##NAME = "colstat.sh" 
##SYNOPSIS = "colstat.sh [-h] [INT(default 1)]" 
##DATE = "2012"
##AUTHORS = "Jean Marc Aury, Genoscope CEA. Maria Bernard"
##KEYWORDS = "compute statistcs on integer column"
##DESCRIPTION = "colstat index_col"


pg=`basename $0`

usage() {
    echo USAGE :
    echo "       $pg <field number : int>"
    echo "       Return statistics from a set of value (stdin). Optionnal argument is the field where to find the dataset, 1 by default."
    exit 1
}

field=1

while [ $1 ]
do
    case $1 in
      "-h") usage ;;
      *) field=$1 ;;
    esac 
    shift
done

gawk -v field=$field '

BEGIN {
  if(field=="") { field=1; }
}

NF<field {
  printerr("Not enough field : NF="NF" and field="field"\n");
  exit;
}

NF>0 {
  n++;
  s+=$field;
  a[n]=$field;

}

n==1 { 
  min=$field;
  max=$field; 
}


NF>0 {
  if ($field<min) min=$field;
  if ($field>max) max=$field;
}


END {
  # Calcul de la moyenne
  mean=0;
  if(n > 0) { mean=s/n ; }

  # calcul de l ecart type
  for (i=1; i<=n; i++ ) {
    x=a[i];
    sum_ecart += ( (x-mean) * (x-mean ) )
  }   
  SD=0;
  if(n>0) { SD=sqrt(sum_ecart / n) ; }

  # calcul de la mediane
  nbelem = asort(a);
  med = a[int((nbelem+1)/2)];

  printf("%s %d   %s %d   %s %.2f   %s %.2f   %s %.2f   %s %.2f   %s %.2f\n",   "Nombre=",n,"Somme=",s,"Moyenne=",mean,"SD=",SD,"max=",max, "min=", min, "Mediane=", med);
}


function printerr(message) {
  printf("%s\n", message) > "/dev/fd/2";
}'
