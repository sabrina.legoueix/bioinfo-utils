#!/usr/bin/perl -w

use Pod::Usage;
use Getopt::Long;

my($opt_help, $opt_man);

GetOptions(
	'help' => \$opt_help,
	'man'  => \$opt_man,
)
or pod2usage( "Try '$0 --help' for more information.");

pod2usage( -verbose => 1 ) if $opt_help;
pod2usage( -verbose => 2 ) if $opt_man;

sub goodAC( $ \% )
  {
    local ($s, $AC) = @_;
    return 1 if (exists($$AC{$s}));
    return 0;
  }

#------------------------------------------------------------
  
pod2usage(-msg => 'Wrong number of params') unless ($#ARGV >= 0);

if (open(FIN,$ARGV[0])) # file or command providing a list of AC
  {
    while (<FIN>)
      {
	chomp;
	$AC{$_}='';
      }
    close(FIN);
  }
else # AC to be extracted are given as parameters
  {
    map{$AC{$_}='';} @ARGV;
  }

$extract=0;
while (<STDIN>)
  {
    chomp;
    if (($s) = (/^>(\S+)/))
      {
	if ($extract == 1) { $extract = 0;}
	$extract = &goodAC($s,\%AC);
      }
    if ($extract == 1)
      {
	print "$_\n";
      }
  }

=pod

=head1 NAME

 fasta_extract.pl

=head1 SYNOPSIS

 fasta_extract.pl <ACs|AC_file> < file.fa

=head1 DESCRIPTION

 Read a FASTA file as STDIN
 If parameter 1 is a file, consider it as a list of accession numbers,
 otherwise consider all paramters as accession numbers of sequences
 to be extrated from the FASTA file

=head1 AUTHORS

 Patrice Dehais

=head1 VERSION

 1

=head1 DATE

 2013

=head1 KEYWORDS

 fasta extract

=head1 EXAMPLE

 cat all.fa | fasta_extract.pl some.lst > some.fa

=cut

