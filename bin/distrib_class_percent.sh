#!/bin/sh

##NAME = "distrib_class_percent.sh" 
##SYNOPSIS = "distrib_class_percent.sh [-h] [<SIZE_OF_BIN>INT(default 10)] [<COLUMN_INDEX>INT(default 1)]" 
##DATE = "2012"
##AUTHORS = "Jean Marc Aury, Genoscope CEA. Maria Bernard"
##KEYWORDS = "compute histogram distribution on integer column transform in proportion (percent)"
##DESCRIPTION = "distrib_class_percent <window : int> <field number : int>"


pg=`basename $0`

usage() {
    echo USAGE :
    echo "       $pg <window : int> <field number : int>"
    echo "       Return a bin distribution (y axes = % of cases) from a set of values (stdin)"
    echo "         First argument is the size of the bin, 10 by default"
    echo "         Second argument is the field where to find the dataset, 1 by default"
    exit 1
}

WIN=
FIELD=

while [ $1 ]
do
    case $1 in
      "-h") usage ;;
      *) if [ "$WIN" = "" ]; then WIN=$1; else FIELD=$1; fi ;;
    esac 
    shift
done

gawk -v win=$WIN -v field=$FIELD 'BEGIN { if(win=="") { win=10; } if(field == "") { field=1; } mul=1; while(win < 1) { win=win*10; mul=mul*10; }} { offset=1; if(val < 0) { offset = -1; } val = int($field*mul); if(val%win == 0 ) { idp[val]++; } else { idp[int((val/win)+offset)*win]++; } tot++; } END{ for(i in idp) { print i/mul, (idp[i]/tot)*100; } }' | sort -k1,1n
