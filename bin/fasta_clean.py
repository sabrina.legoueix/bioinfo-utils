#!/bin/env python

from Bio import SeqIO
import sys
import string
import argparse

__name__ = "fasta_clean.py"
__synopsis__ = "clean_fasta.py -f file_name.fasta -l line_length > STDOUT"
__date__ = "2012/10"
__authors__ = "Christophe Klopp"
__keywords__ = "fasta line lenght"
__description__ = "This script sets the lines of a fasta file to the same lenght (second parameter)."

parser=argparse.ArgumentParser(description=__description__)
parser.add_argument('-f', action='store', dest='fasta_file',  help='name of the fasta file', required=True)
parser.add_argument('-l', action='store', dest='lenght', type=int, default=60, help='lenght of the line in the cleaned fasta file')
parser.add_argument('--version', action='version', version='%(prog)s 0.0')
args=parser.parse_args()

def split_len(seq, length):
    return [seq[i:i+length] for i in range(0, len(seq), length)]

for seq_record in SeqIO.parse(open(args.fasta_file), "fasta") :
	print ">"+seq_record.id
        for s in split_len(seq_record.seq.tostring(), args.lenght) :
            print s

handle.close()


