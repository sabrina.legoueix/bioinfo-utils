#!/usr/bin/perl
#use strict;
use warnings;
use Getopt::Long;
use Pod::Usage;
my %options;
my %h_type = (
    'SNP'       => 1,
    'INDEL'     => 1,
);

Getopt::Long::Configure("no_ignorecase");
Getopt::Long::Configure("bundling");
GetOptions(\%options,
	'man',
	'h|help',
	'f|file=s',
	't|type=s',
	'o|output=s'
)
or pod2usage(-verbose => 1);

pod2usage(-verbose => 2) if (exists $options{'man'});
pod2usage(-verbose => 1) if (exists $options{'h'});
pod2usage(-message => "Please provide A VCF file ! (-f)",-verbose => 1) if (! exists $options{'f'});
pod2usage(-message => "Please specify if you want to extract SNP or INDEL ! (-t)",-verbose => 1) if (! exists($options{'t'}) || ! exists $h_type{$options{'t'}});
#pod2usage(-message => "Please specify the output file ! (-o)",-verbose => 1) if (! exists $options{'o'});



my $output = *STDOUT;
my $file=$options{'f'};
my $type=$options{'t'};

if (exists ($options{'o'})){
	$output = $options{'o'};
	open($output, ">$output") or die "can't open $output: $!\n";
}


my @line;

open FILE,$file or die "Cannot open file $file\n";
while(<FILE>){
	chomp();
	if(/^#/){
		print $output $_,"\n";
		next;
	}
	# SNP case
	if ($type eq 'SNP'){
		@line = split(/\s/,$_);
		if ( (length($line[3]) == 1 && length($line[4]) == 1) || (length($line[3]) == 1 && $line[4] =~ m/[ACGT],[ACGT]/ && length($line[4]) == 3) ){
			for (my $i=0;$i<=2;$i++){
				print $output $line[$i],"\t";
			}
			for (my $i=3;$i<scalar(@line)-1;$i++){
				print $output $line[$i],"\t";
			}
			print $output $line[scalar(@line)-1],"\n";
		}
		else{
			next;
		}
	}
	# INDEL case
	else {
		@line = split(/\s/,$_);
		next if ( (length($line[3]) == 1 && length($line[4]) == 1) || (length($line[3]) == 1 && $line[4] =~ m/[ACGT],[ACGT]/ && length($line[4]) == 3) );
		for (my $i=0;$i<=2;$i++){
			print $output $line[$i],"\t";
		}
		for (my $i=3;$i<scalar(@line)-1;$i++){
			print $output $line[$i],"\t";
		}
		print $output $line[scalar(@line)-1],"\n";
	}
}

close FILE;


=pod

=head1 NAME

extract_variants_from_VCF.pl

=head1 SYNOPSIS

extract_variants_from_VCF.pl -f FILE -t [SNP|INDEL] [options]

=head1 OPTIONS

-o <FILE>	: output FILE

=head1 DESCRIPTION

Extracts SNP or INDEL from a VCF file.

=head1 AUTHORS

Olivier Rue

=head1 VERSION

1

=head1 DATE

06/2012

=head1 KEYWORDS

VCF, extract, SNP, INDEL, variants

=head1 EXAMPLE

./extract_variants_from_VCF.pl -f snp_and_indel.vcf -t SNP -o only_snp.vcf

=cut
