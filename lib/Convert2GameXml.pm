package Convert2GameXml ;

=pod

=head1 NAME

	Convert2GameXml - Object which read blast data, gff, or gth data and convert it in GameXml

=head1  SYNOPSIS
	
	#Create the object
	my $convert = Convert2GameXml->New();
	#Initialize xml element
	$convert->Initialize($seq_file,$output_file);
	#Add several analysis
	$convert->generateComputationnalXML($blast_file,"blast");
	$convert->generateComputationnalXML($gff_file,"gff");
	
	#Wrote in output file xml data
	$convert->writeOutfile();
	
=head1 DESCRIPTION
	
	For a sequence file, several analysis file can be add.
	This analysis file will create a node computationnal-analysis.

=cut



use strict;
use Bio::SearchIO;
use Bio::SeqIO;
use ParamParser;
use File::Glob;

use XML::Element;

my $VERSION = 0.1;


=head2 function

 Title        : New
 Usage        : my $convert = Convert2GameXml->New();
 Prerequisite : none
 Function     : none
 Returns      : Convert2GameXml object
 Args         : none

=cut


sub New
{
	my ($class) = @_;
	my $self = {
		__blast_file => [],
		__seq_file => "",
		__out_file =>"",
		__xml_root=>ref,
		__query_name=>""
	};
	bless($self, $class);
	return ($self);
}


=head2 function

 Title        : Initialize
 Usage        : $convert->Initialize($seq_file,$output_file);
 Prerequisite : $seq_file exists
 				path to $output_file exists
 Function     : Initialize attributes and create element sequence from the sequence file
 Returns      : none
 Args         : $seq_file : fasta sequence file
 				output_file : file name of output file

=cut

sub Initialize
{
	my ($self,$seq_file,$output,$organism)=@_;
	$self->{'__seq_file'}=$seq_file;
	$self->{'__out_file'}=$output;
	$self->{'__xml_root'}= XML::Element->new('game');
	$self->__initQuery($organism);
}

=head2 function

 Title        : __initQuery
 Usage        : $self->__initQuery();
 Prerequisite : $self->{'__seq_file'} exists
 Function     : Initialize query xml element sequence from the sequence file
 Returns      : none
 Args         : none

=cut

sub __initQuery
{
	my ($self, $organism)=@_;
	my $in1 = new Bio::SeqIO(-format => 'fasta', -file => $self->{'__seq_file'}); 
	my $seq=$in1->next_seq;
	$in1->close(); 
	$self->{'__query_name'}=$seq->id();

	my $seqxml = XML::Element->new('seq', 'id'=>$seq->id(), 'length'=>$seq->length(),'type'=>"dna",'focus'=>"true" );
	$self->{'__xml_root'}->push_content($seqxml);

	# element de seq
	my $namexml = XML::Element->new('name');
	$namexml->push_content($seq->id());
	$seqxml->push_content($namexml);
	my $organismxml=XML::Element->new('organism');
	$organismxml->push_content($organism);
	$seqxml->push_content($organismxml);
	my $residuesxml=XML::Element->new('residues');
	$residuesxml->push_content($seq->seq());
	$seqxml->push_content($residuesxml);
	$seq->DESTROY();
	
}

=head2 function

 Title        : getQueryName

=cut


sub getQueryName
{
        my ($self)=@_;
        return ($self->{'__query_name'});
}

=head2 function

 Title        : generateComputationnalXML
 Usage        : $convert->generateComputationnalXML($blast_file,"blast");
 Prerequisite : $blast_file exists
 				type is [blast|gff|gth]
 Function     : Create a computationnal analysis xml element and the sequence element corresponding to the subject
 Returns      : none
 Args         : $blast_file	blast ouput file
 				type of analysis or format data.

=cut


sub generateComputationnalXML
{
	my ($self,$ref_files, $type,$min_len,$min_pci,$max_evalue )=@_;
	push (@{$self->{'__blast_file'}},$ref_files);
	if ($type eq "blast")
	{
		$self->__generateComputationnalBlastXML($ref_files,$min_len,$min_pci,$max_evalue);
	}
	elsif ($type eq "gff")
	{
		$self->__generateComputationnalGffXML($ref_files);
	}
}



=head2 function

 Title        : __generateComputationnalGffXML
 Usage        : $convert->__generateComputationnalGffXML($file);
 Prerequisite : $file exists
 Function     : Create a computationnal analysis xml element and the sequence element corresponding to the subject from a gff file
 Returns      : none
 Args         : $file	gff ouput file

=cut

sub __generateComputationnalGffXML
{
	my ($self,$tab_files)=@_;

	my $computational_analysis=XML::Element->new('computational_analysis');
	my $programxml=XML::Element->new('program');
	my $databasexml=XML::Element->new('database');

	my $previous="";
	my $first=1;
	
	my $namexml=XML::Element->new('name');
	$namexml->push_content(" ");

	my $result_setxml ;
	for  my $file ( @{$tab_files} )
        {
		print "\t -read : $file \n"; 
		open IN , $file or die "Unable to open file $file";
	 	while( my $result = <IN>)
		{
	
			my ($cdna_id,$bnk,$prg,$gen_begin,$gen_end,$match_len,$strand_gen,$score,$cdna_idb,$cdna_begin,$cdna_end)=split (" ",$result);
	
			# Au premier passage on fixe les variable du programme et de la banque.
			if ($first)
			{
				$programxml->push_content($prg);
				$databasexml->push_content($bnk);
				$first=0;
			}
	
			# si on change de hit -> creation d'un nouveau result set
			if ($cdna_id ne $previous)
			{
			    $computational_analysis->push_content($result_setxml);
				$result_setxml = XML::Element->new('result_set');
				$previous=$cdna_id;
				$namexml->delete_content();
				$namexml->push_content($cdna_id);
				$result_setxml->push_content($namexml);
				
				
				
				# ajout des basilse "seq" pour tout les subject
				my $subjectxml = XML::Element->new('seq', 'id'=>$cdna_id);
				
				#Debut gestion des element seq (subject) sous la racine du document
				 $subjectxml->push_content($namexml->clone());
				 $self->{'__xml_root'}->push_content($subjectxml);
				#Fin gestion des element seq
			}
			
	
				#creation d'un result_span par hsp <-> par ligne
				my $result_spanxml = XML::Element->new('result_span');
				$result_spanxml->push_content($namexml->clone());
	
				my $hsp_output_s = XML::Element->new('output');
				my $hsp_value_s = XML::Element->new('value');
				my $hsp_type_s = XML::Element->new('type');
				$hsp_type_s->push_content("match_len");
				#score du hsp		
				$hsp_value_s->push_content($match_len);
				$hsp_output_s->push_content($hsp_type_s);
				$hsp_output_s->push_content($hsp_value_s);
				# ajout des scores au result_span
				$result_spanxml->push_content($hsp_output_s);
				my $scorexml= XML::Element->new('score');
			    $scorexml->push_content($score);
				$result_spanxml->push_content($scorexml);
				##################################################
				# coordonnees query :
				my $seq_relationship_query = XML::Element->new('seq_relationship', 'type'=>"query", 'seq'=>$self->{'__query_name'});
				my $span_query = XML::Element->new('span');
				my ($gen_begin,$gen_end) = ($strand_gen eq "-")?($gen_end,$gen_begin):($gen_begin,$gen_end);

				my $start_query = XML::Element->new('start');
				$start_query->push_content($gen_begin);
				my $end_query = XML::Element->new('end');
				$end_query->push_content($gen_end);

				$span_query->push_content($start_query);
				$span_query->push_content($end_query);
				$seq_relationship_query->push_content($span_query);
		
		
				##################################################
				# coordonnees subject :				
				my $seq_relationship_subject = XML::Element->new('seq_relationship', 'type'=>"subject", 'seq'=>$cdna_id);
				my $span_subject = XML::Element->new('span');
				my $start_subject = XML::Element->new('start');
				$start_subject->push_content($cdna_begin);
				my $end_subject = XML::Element->new('end');
				$end_subject->push_content($cdna_end);
				$span_subject->push_content($start_subject);
				$span_subject->push_content($end_subject);
				$seq_relationship_subject->push_content($span_subject);
			
				$result_spanxml->push_content($seq_relationship_subject);
				$result_spanxml->push_content($seq_relationship_query);
				$result_setxml->push_content($result_spanxml);
				
      	}

	close IN;
	}
	$computational_analysis->push_content($result_setxml);
	$computational_analysis->push_content($programxml);
	$computational_analysis->push_content($databasexml);
	$self->{'__xml_root'}->push_content($computational_analysis);
}


=head2 function

 Title        : __generateComputationnalBlastXML
 Usage        : $convert->__generateComputationnalBlastXML($file);
 Prerequisite : $file exists
 Function     : Create a computationnal analysis xml element and the sequence element corresponding to the subject from a blast file
 Returns      : none
 Args         : $file	blast ouput file

=cut

sub __generateComputationnalBlastXML
{

	my ($self,$tab_files, $min_len,$min_pci,$max_evalue)=@_;

	my $computational_analysis=XML::Element->new('computational_analysis');
	my $programxml=XML::Element->new('program');
	my $databasexml=XML::Element->new('database');

	for  my $file ( @{$tab_files} )
	{
	
		print "\t -read : $file \n"; 
		my $in = new Bio::SearchIO(-format => 'blast', -file => $file); 
		while( my $result = $in->next_result )
		{
			my $db= $result->database_name();
			$programxml->push_content($result->algorithm());

			$databasexml->push_content($result->database_name());
   			while( my $hit = $result->next_hit )
   			{
				# Rennome certain Id pour que le lien "Get info via the web fonctionne
				my $id=$hit->name();
			
				$id=~s/\.\d//; # Ani et Swissprot on supprime la version
				$hit->name($id);
				# ajout des basilse "seq" pour tout les subject
				my $subjectxml = XML::Element->new('seq', 'id'=>$hit->name(), 'length'=>$hit->length());
				
				#Debut gestion des element seq (subject) sous la racine du document
				 my $namexml=XML::Element->new('name');
				 $namexml->push_content($hit->name());
				 $subjectxml->push_content($namexml);
			
			 
				 my $descriptionxml=XML::Element->new('description');
				 my $d=$hit->description();
				 chomp($d);
				 $d=~s/\W*?([\w\s\.:;=-_|%]*)\W*?/$1/g;
				 $descriptionxml->push_content($d);
				 $subjectxml->push_content($descriptionxml);
	
				 $self->{'__xml_root'}->push_content($subjectxml);
				#Fin gestion des element seq (
				
				# creation du resultat : 1 result set par banque ou programme , on ne traite qu'un resultat blast contre une banque donc OK
				my $result_setxml = XML::Element->new('result_set');
				$result_setxml->push_content($namexml);
				# gestion des score : evalue + score par result_set
				 my $hit_output_e = XML::Element->new('output');
				 my $hit_output_s = XML::Element->new('output');
				 my $hit_value_e = XML::Element->new('value');
				 my $hit_value_s = XML::Element->new('value');
				 #evalue du hit
				 #type de sortie : score ou evalue
				 my $type_e = XML::Element->new('type');
				 my $type_s = XML::Element->new('type');

				 #evalue
				 $type_e->push_content("expect");
				 $hit_value_e->push_content($hit->significance);
				 $hit_output_e->push_content($type_e);
				 $hit_output_e->push_content($hit_value_e);

				 #bit score
				 $type_s->push_content("score");
				 $hit_value_s->push_content($hit->score);
				 $hit_output_s->push_content($type_s);
				 $hit_output_s->push_content($hit_value_s);

				 $result_setxml->push_content($hit_output_e);
				 $result_setxml->push_content($hit_output_s);
				# fin gestion des scores
			
		
	       			while( my $hsp = $hit->next_hsp )
			 	{
					if( ($min_len=="" && $max_evalue=="" && $min_pci=="") ||( ( $hsp->length('total') > $min_len  ) && ( $hsp->percent_identity >= $min_pci ) && ( $hsp->evalue <= $max_evalue ) ) )
					{	
					#creation d'un relult_span par hsp
					my $result_spanxml = XML::Element->new('result_span');
					$result_spanxml->push_content($namexml->clone());
					my $scorexml= XML::Element->new('score');
				    	$scorexml->push_content($hsp->score);
					$result_spanxml->push_content($scorexml);
					my $hsp_output_e = XML::Element->new('output');
					my $hsp_output_s = XML::Element->new('output');
					my $hsp_value_e = XML::Element->new('value');
					my $hsp_value_s = XML::Element->new('value');
					#type de sortie : score ou evalue
					my $hsp_type_e = XML::Element->new('type');
					my $hsp_type_s = XML::Element->new('type');

					$hsp_type_e->push_content("expect");
					$hsp_type_s->push_content("score");
				
					#evalue du hsp
					$hsp_value_e->push_content($hsp->significance);
					$hsp_output_e->push_content($hsp_type_e);
					$hsp_output_e->push_content($hsp_value_e);
					#score du hsp		
					$hsp_value_s->push_content($hsp->score);
					$hsp_output_s->push_content($hsp_type_s);
					$hsp_output_s->push_content($hsp_value_s);
					# ajout des scores au result_span
					$result_spanxml->push_content($hsp_output_e);
					$result_spanxml->push_content($hsp_output_s);
				
					##################################################
					# coordonnees query :
					my $seq_relationship_query = XML::Element->new('seq_relationship', 'type'=>"query", 'seq'=>$self->{'__query_name'});
					my $span_query = XML::Element->new('span');
					my ($start,$end) = ($hsp->strand<0)?($hsp->end,$hsp->start):($hsp->start,$hsp->end);

					my $start_query = XML::Element->new('start');
					$start_query->push_content($start);
					my $end_query = XML::Element->new('end');
					$end_query->push_content($end);

					$span_query->push_content($start_query);
					$span_query->push_content($end_query);
					$seq_relationship_query->push_content($span_query);
				
					my $algmt_query = XML::Element->new('alignment');
					$algmt_query->push_content($hsp->query_string());

					$seq_relationship_query->push_content($algmt_query);
				
					##################################################
					# coordonnees subject :				
					my $seq_relationship_subject = XML::Element->new('seq_relationship', 'type'=>"subject", 'seq'=>$hit->name());
					my $span_subject = XML::Element->new('span');
					my $start_subject = XML::Element->new('start');
					$start_subject->push_content($hsp->hit()->start);
					my $end_subject = XML::Element->new('end');
					$end_subject->push_content($hsp->hit()->end);
					$span_subject->push_content($start_subject);
					$span_subject->push_content($end_subject);
					$seq_relationship_subject->push_content($span_subject);
					my $algmt_subject = XML::Element->new('alignment');
					$algmt_subject->push_content($hsp->hit_string());
					$seq_relationship_subject->push_content($algmt_subject);
				
				
					$result_spanxml->push_content($seq_relationship_subject);
					$result_spanxml->push_content($seq_relationship_query);
					$result_setxml->push_content($result_spanxml);
							
					}#end if	
		      		}
				$computational_analysis->push_content($result_setxml);
	   		}
		}
		$in->close(); 
	}	
	$computational_analysis->push_content($programxml);
	$computational_analysis->push_content($databasexml);
	$self->{'__xml_root'}->push_content($computational_analysis);
	
}


sub __generateComputationnalGthXML
{
	my ($self,$file)=@_;
	my $in = new Bio::SearchIO(-format => 'blast', -file => $file); 

	my $computational_analysis=XML::Element->new('computational_analysis');
	
	my $programxml=XML::Element->new('program');
	my $databasexml=XML::Element->new('database');
	open(GTH,"$file") or die "Unable to open >$file<\n";
my $exonok = 0;
	my $seqok = 0;
	my $match_len = 0;
	my $curr_pci = 0;
	my $curr_pcs = 0;
	my @a_exon=();
	my $strand_cdna="";
	my $strand_gen="";
	my $mrna_id = "";
	my $est_len=0;
	my %h_mrna=();
	my $bankname="toto";
	while(my $lign=<GTH>)
	{
		chomp($lign);
		next if ( $lign =~ /^\s*$/ );
		next if ( $lign =~ /Intron/ );
		if ( $lign =~ /^Predicted gene structure/ || $lign =~ /^Genomic/) # pas de .+ apres structure /GeneSeqer
		{
			($exonok,$seqok) = (1,0);
			next;
		}
		if ( $lign =~ /^MATCH\s+/ )
		{
		#MATCH   42096.25-       27662_GC+       0.902   849     0.982   C
			my @a_F = split(' ',$lign);
			$curr_pci = $a_F[3] * 100;
			$curr_pcs = $a_F[5] * 100;
			$match_len = $a_F[4];
			my $score_hit = $a_F[5];
			if ( $mrna_id eq '' )	# if the est id contains ':' gth reports only the part before the : (ex: gb:DW018045 -> gb)
			                        # in order to patch this bug, I read the mrna id from the "^EST Sequence" line which contain the correct id
			{
				$mrna_id = $a_F[2];
				$mrna_id =~ s/[\-\+]$//;
			}
			($strand_cdna) = ( $a_F[2] =~ /([-\+])$/ );
			($strand_gen) = ( $a_F[1] =~ /([-\+])$/ ) ;
		}
		if ( $lign =~ /^Alignment.+:\s*$/ )
		{
			if ( $#a_exon != -1 )
			{
			#	print "AVANT : \n @a_exon";
				my (@a_exon) = &ReformatGff($match_len,$strand_gen,$mrna_id, $strand_cdna,$est_len,$bankname,@a_exon);
				$h_mrna{$mrna_id} = "$match_len $strand_cdna $strand_gen";
				print EST @a_exon;
			#	print "\nAPRES : \n @a_exon";				
			}
			$match_len = 0;
			@a_exon=();
			$exonok = 0;
			$strand_gen=0;
			$strand_cdna=0;
			$mrna_id='';
			$est_len=0;
			next;
		}
		if ( $seqok ) 
		{
			my ($seq) = $lign;
			$seq =~ s/[^A-Za-z]//g;
			$est_len+=length($seq);
		}
		if ( $lign =~ /^EST sequence/i )
		{
			($mrna_id) = $lign =~ /description=(\S+)/;
			$seqok = 1;
			next;
		}
		next if ( ! $exonok );
		push @a_exon, $lign if ( $lign =~ /^\s*Exon/ );

	}
	close(GTH);
	while( my $result = $in->next_result )
	{
		my $db= $result->database_name();
		$programxml->push_content($result->algorithm());

		$databasexml->push_content($result->database_name());
   		while( my $hit = $result->next_hit )
   		{
			# Rennome certain Id pour que le lien "Get info via the web fonctionne
			my $id=$hit->name();
			
			$id=~s/Afu/AFUA_/;# afumigatus 
			$id=~s/\.\d//; # Ani et Swissprot on supprime la version
			$hit->name($id);
			# ajout des basilse "seq" pour tout les subject
			my $subjectxml = XML::Element->new('seq', 'id'=>$hit->name(), 'length'=>$hit->length());
			
			#Debut gestion des element seq (subject) sous la racine du document
			 my $namexml=XML::Element->new('name');
			 $namexml->push_content($hit->name());
			 $subjectxml->push_content($namexml);
			
			 
			 my $descriptionxml=XML::Element->new('description');
			 my $d=$hit->description();
			 chomp($d);
			 $d=~s/\W*?([\w\s\.:;=-_|%]*)\W*?/$1/g;
			 $descriptionxml->push_content($d);
			 $subjectxml->push_content($descriptionxml);

			 $self->{'__xml_root'}->push_content($subjectxml);
			#Fin gestion des element seq (
			
			# creation du resultat : 1 result set par banque ou programme , on ne traite qu'un resultat blast contre une banque donc OK
			my $result_setxml = XML::Element->new('result_set');
			$result_setxml->push_content($namexml);
			# gestion des score : evalue + score par result_set
			 my $hit_output_e = XML::Element->new('output');
			 my $hit_output_s = XML::Element->new('output');
			 my $hit_value_e = XML::Element->new('value');
			 my $hit_value_s = XML::Element->new('value');
			 #evalue du hit
			 #type de sortie : score ou evalue
			 my $type_e = XML::Element->new('type');
			 my $type_s = XML::Element->new('type');

			 #evalue
			 $type_e->push_content("expect");
			 $hit_value_e->push_content($hit->significance);
			 $hit_output_e->push_content($type_e);
			 $hit_output_e->push_content($hit_value_e);

			 #bit score
			 $type_s->push_content("score");
			 $hit_value_s->push_content($hit->score);
			 $hit_output_s->push_content($type_s);
			 $hit_output_s->push_content($hit_value_s);

			 $result_setxml->push_content($hit_output_e);
			 $result_setxml->push_content($hit_output_s);
			# fin gestion des scores
			
		
       		while( my $hsp = $hit->next_hsp )
	   		{
				#creation d'un relult_span par hsp
				my $result_spanxml = XML::Element->new('result_span');
				$result_spanxml->push_content($namexml->clone());
				my $scorexml= XML::Element->new('score');
			    $scorexml->push_content($hsp->score);
				$result_spanxml->push_content($scorexml);
				my $hsp_output_e = XML::Element->new('output');
				my $hsp_output_s = XML::Element->new('output');
				my $hsp_value_e = XML::Element->new('value');
				my $hsp_value_s = XML::Element->new('value');
				#type de sortie : score ou evalue
				my $hsp_type_e = XML::Element->new('type');
				my $hsp_type_s = XML::Element->new('type');

				$hsp_type_e->push_content("expect");
				$hsp_type_s->push_content("score");
				
				#evalue du hsp
				$hsp_value_e->push_content($hsp->significance);
				$hsp_output_e->push_content($hsp_type_e);
				$hsp_output_e->push_content($hsp_value_e);
				#score du hsp		
				$hsp_value_s->push_content($hsp->score);
				$hsp_output_s->push_content($hsp_type_s);
				$hsp_output_s->push_content($hsp_value_s);
				# ajout des scores au result_span
				$result_spanxml->push_content($hsp_output_e);
				$result_spanxml->push_content($hsp_output_s);
				
				
				######################
				#Frame
				#my $hsp_output_frame = XML::Element->new('output');
				#my $hsp_value_frame = XML::Element->new('value');
				#my $hsp_type_frame = XML::Element->new('type');
				#$hsp_value_frame->push_content( $hsp->frame('query'));
				#$hsp_type_frame->push_content("query_frame");
				#$hsp_output_frame->push_content($hsp_type_frame);
				#$hsp_output_frame->push_content($hsp_value_frame);
				#$result_spanxml->push_content($hsp_output_frame);
				
				##################################################
				# coordonnees query :
				my $seq_relationship_query = XML::Element->new('seq_relationship', 'type'=>"query", 'seq'=>$self->{'__query_name'});
				my $span_query = XML::Element->new('span');
				my ($start,$end) = ($hsp->strand<0)?($hsp->end,$hsp->start):($hsp->start,$hsp->end);

				my $start_query = XML::Element->new('start');
				$start_query->push_content($start);
				my $end_query = XML::Element->new('end');
				$end_query->push_content($end);

				$span_query->push_content($start_query);
				$span_query->push_content($end_query);
				$seq_relationship_query->push_content($span_query);
				
				my $algmt_query = XML::Element->new('alignment');
				$algmt_query->push_content($hsp->query_string());

				$seq_relationship_query->push_content($algmt_query);
				
				##################################################
				# coordonnees subject :				
				my $seq_relationship_subject = XML::Element->new('seq_relationship', 'type'=>"subject", 'seq'=>$hit->name());
				my $span_subject = XML::Element->new('span');
				my $start_subject = XML::Element->new('start');
				$start_subject->push_content($hsp->hit()->start);
				my $end_subject = XML::Element->new('end');
				$end_subject->push_content($hsp->hit()->end);
				$span_subject->push_content($start_subject);
				$span_subject->push_content($end_subject);
				$seq_relationship_subject->push_content($span_subject);
				my $algmt_subject = XML::Element->new('alignment');
				$algmt_subject->push_content($hsp->hit_string());
				$seq_relationship_subject->push_content($algmt_subject);
				
				
				$result_spanxml->push_content($seq_relationship_subject);
				$result_spanxml->push_content($seq_relationship_query);
				$result_setxml->push_content($result_spanxml);
							
	
      		}
			$computational_analysis->push_content($result_setxml);
   		}
	}
	$computational_analysis->push_content($programxml);
	$computational_analysis->push_content($databasexml);
	$self->{'__xml_root'}->push_content($computational_analysis);
	$in->close(); 
	
}


sub getComputationnalAnalysis
{
	my ($self)=@_;
	return $self->{'__xml_root'}->content('');
}

sub writeOutfile
{
	my ($self)=@_;

	open OUT, ">".$self->{'__out_file'};
	print OUT "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>
	   <!-- DOCTYPE game SYSTEM \"GAME.dtd\" -->";
	print OUT $self->{'__xml_root'}->as_XML();
	close OUT;
}
1;

