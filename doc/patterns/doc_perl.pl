use Getopt::Long;
use Pod::Usage;

my( $opt_help, $opt_man, $opt_full, $opt_admins, $opt_choose, $opt_createdb );

GetOptions(
    'help'                 =>      \$opt_help,
    'man'                  =>      \$opt_man,
)
  or pod2usage( "Try '$0 --help' for more information.");

pod2usage( -verbose => 1 ) if $opt_help;
pod2usage( -verbose => 2 ) if $opt_man;

# Main
print "Coucou";

=pod

=head1 NAME

mon_script.pl

=head1 SYNOPSIS

mon_script.pl [options] > STDOUT

=head1 DESCRIPTION

First line must be explicit (will be in web site)
After write what you want

=head1 AUTHORS

Toto

=head1 VERSION

1

=head1 DATE

05/2012

=head1 KEYWORDS

bidule bidou

=cut
