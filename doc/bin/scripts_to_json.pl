#!/usr/bin/perl
use strict;
use warnings;
use Cwd;
use Getopt::Long;
use Pod::Usage;

my %options;
my $cur_dir=cwd();
my $first=1;
my $nb_scripts=0;
my $nb_correct_scripts=0;


Getopt::Long::Configure("no_ignorecase");
    Getopt::Long::Configure("bundling");
    
    GetOptions(	\%options,
		'file|f=s',
		'directory|d=s',
		'help|h',
		'man') or pod2usage(-verbose => 1);

	pod2usage(-verbose => 1,-noperldoc => 1) if (exists $options{'help'});
	pod2usage(-verbose => 2,-noperldoc => 1) if (exists $options{'man'});
	pod2usage(-message => "A file or a directory must be provided ! (-f or -d)",-verbose => 1) if ( (!exists $options{'file'}) && (!exists $options{'directory'}) );


####STORE WRONG FILES#################
open (WRONG,">wrong_scripts.txt");
######################################

####PREPARE JSON FILE#################
open(JSON,">out.json") or die "Unable to open out.json\n";
print JSON "{\"scriptotheque\":{";
######################################

    
### SCAN ONE FILE
if(exists($options{'file'})){
    my $file = $options{'file'};
    print "Scanning file ",$file,"\n";
    my $fic=$file;
    &scan_file($fic,1);
    
}
else{
    $cur_dir=$options{'directory'};
    print "Scanning ",$cur_dir,"\n";
    opendir(my $dh, $cur_dir);
    my @files = grep { -f "$cur_dir/$_" } readdir($dh);
    
    foreach my $fic (@files){
      &scan_file($cur_dir."/".$fic, scalar(@files));
    }
}



print JSON "}\n}";
close JSON;
system 'sed -i -e "s/>/\&#62;/g" -e "s/</\&#60;/g" out.json';

close WRONG;

my $doc_perl = "\n\nEXAMPLE OF DOCUMENTATION FOR PERL SCRIPTS : \
#!/usr/bin/perl

use strict;
use warnings;
use Getopt::Long;
use Pod::Usage;

my( \$opt_help, \$opt_man, \$opt_full, \$opt_admins, \$opt_choose, \$opt_createdb );

GetOptions(
    'help'                 =>      \$opt_help,
    'man'                  =>      \$opt_man,
)
  or pod2usage( \"Try '\$0 --help' for more information.\");

pod2usage( -verbose => 1 ) if \$opt_help;
pod2usage( -verbose => 2 ) if \$opt_man;

# Main
print \"Coucou\";

\=pod

\=head1 NAME

mon_script.pl

\=head1 SYNOPSIS

mon_script.pl [options]

\=head1 DESCRIPTION

mon_script.pl prints Coucou

\=head1 AUTHORS

Author

\=head1 VERSION

1

\=head1 DATE

05/2012

\=head1 KEYWORDS

bidule bidou

\=head1 EXAMPLE

perl mon_script.pl > STDOUT

\=cut
";

my $doc_python = "\n\nEXAMPLE OF DOCUMENTATION FOR PYTHON SCRIPTS : \
__name__ = \"mon_script.py\"
__synopsis__ = \"mon_script.py |OPTIONS]\"
__example__ = \"python mon_script.py > STDOUT\"
__date__ = \"05/2012\"
__authors__ = \"Author\"
__keywords__ = \"bidule bidou\"
__description__ = \"mon_script.pl prints Coucou\"
";

my $doc_bash = "\n\nEXAMPLE OF DOCUMENTATION FOR BASH SCRIPTS : \
##NAME = \"mon_script.sh\"
##SYNOPSIS = \"mon_script.sh |OPTIONS]\"
##EXAMPLE = \"sh mon_script.sh > STDOUT\"
##DATE = \"05/2012\"
##AUTHORS = \"Author\"
##KEYWORDS = \"bidule bidou\"
##DESCRIPTION = \"mon_script.sh prints Coucou\"
";

if (-s "wrong_scripts.txt"){
    `rm -f out.json`;
    open (WRONG,">>wrong_scripts.txt");
    print "See wrong_scripts.txt to see bad documented files\n";
    print WRONG "\n-----------------------------------------------------------------------------------------";
    print WRONG $doc_perl;
    print WRONG $doc_python;
    print WRONG $doc_bash;
    close WRONG;
}

#`cp out.json /work/sigenae/OLIVIER/POD/OK2`;



sub scan_file(){
        my $fic=shift;
        my $nbfile=shift;
        my $f="${cur_dir}/$fic";
		my ($name, $synopsis, $date, $authors, $keywords, $description)=("","","","","","");
		my $error_msg="";
		$nb_scripts++;
		
        #### PERL FILES########################################################################################
          if($fic=~/.*\.pl/){
            my $verif = (`podchecker -nowarnings $fic 2>&1 >/dev/null`);
            if ($verif =~ m/syntax OK/){
                
                $name = `podselect -section NAME $fic | sed s/'=head1.*'//`;
                $name =~ s/[\t|\n]//g;
                $synopsis = `podselect -section SYNOPSIS $fic | sed s/'=head1.*'//`;
                $synopsis =~ s/[\t|\n]//g;
                $date = `podselect -section DATE $fic | sed s/'=head1.*'//`;
                $date =~ s/[\t|\n]//g;
                $authors = `podselect -section AUTHORS $fic | sed s/'=head1.*'//`;
                $authors =~ s/[\t|\n]//g;
                $keywords = `podselect -section KEYWORDS $fic | sed s/'=head1.*'//`;
                $keywords =~ s/[\t|\n]//g;
                my $res_description = `podselect -section DESCRIPTION $fic | sed s/'=head1.*'//`;
                my @a_desc = split("\n",$res_description);
                my $i=0;
                while ( $i <= $#a_desc && $description eq "")
                {	
		            my $line = $a_desc[$i];
		            if ($line !~ /^\s+$/ )
		            {
		                $description=$line;
		            }
		            $i ++;
                }
                
                close FIC;
            }
            else{
                $error_msg= $fic." is wrong, use podchecker -nowarnings $fic\n";
            }
          }
          
          #######################################################################################################
          
          #### PYTHON FILES######################################################################################
          if($fic=~/.*\.py/){
            open FIC, "$f" or warn "$f E/S: $!\n";
            
            my $nb_field=0;
            while(<FIC>){
                chomp;
                #print $_,"\n";
                if ($_ =~ m/^__name__/g){
		            ($name) = $_ =~ m/^__name__ = "([^"]+)"/;
		            $nb_field++;
		        }
		        if ($_ =~ m/^__synopsis__/g){
		            ($synopsis) = $_ =~ m/__synopsis__ = "([^"]+)"/;
		            $nb_field++;
		        }
		        if ($_ =~ m/^__date__/g){
		            ($date) = $_ =~ m/__date__ = "([^"]+)"/;
		            $nb_field++;
		        }
		        if ($_ =~ m/^__authors__/g){
		            ($authors) = $_ =~ m/__authors__ = "([^"]+)"/;
		            $nb_field++;
		        }
		        if ($_ =~ m/^__keywords__/g){
		            ($keywords) = $_ =~ m/__keywords__ = "([^"]+)"/;
		            $nb_field++;
		        }
		        if ($_ =~ m/^__description__/g){
		        	($description) = $_ =~ m/__description__ = "([^"]+)"/;
                    $nb_field++;
		        }

            }
			if ($nb_field < 6)
			{
            	$error_msg="$fic is wrong\n";
			}
          }

          #### BASH FILES########################################################################################
          if($fic=~/.*\.sh/){
            open FIC, "$f" or warn "$f E/S: $!\n";
            my $nb_field=0;

            while(<FIC>){
                chomp;

                if ($_ =~ m/^##NAME/gi){
                    ($name) = $_ =~ m/##NAME = "([^"\s]+).*"/gi;
					$nb_field++;
                }
                if ($_ =~ m/^##SYNOPSIS/g){
                    ($synopsis) = $_ =~ m/##SYNOPSIS = "([^"]+)"/gi;
					$nb_field++;
                }
                if ($_ =~ m/^##DATE/g){
                    ($date) = $_ =~ m/##DATE = "([^"]+)"/gi;
					$nb_field++;       
                }
                if ($_ =~ m/^##AUTHORS/g){
                    ($authors) = $_ =~ m/##AUTHORS = "([^"]+)"/gi;
					$nb_field++;           
                }
                if ($_ =~ m/^##KEYWORDS/g){
                    ($keywords) = $_ =~ m/##KEYWORDS = "([^"]+)"/gi;
					$nb_field++;            
                }
                if ($_ =~ m/^##DESCRIPTION/g){
                    ($description) = $_ =~ m/##DESCRIPTION = "([^"]+)"/gi;
					$nb_field++;            
                }
            }
            if ($nb_field<6){
                $error_msg="$fic is wrong";
            }
          close FIC;
          }
          #######################################################################################################
          
          if ($error_msg ne "")
          {
          	
          		my $field ="";
          		$field .="name " if ($name eq "");
          		$field .="synopsis " if ($synopsis eq "");
          		$field .="date " if ($date eq "");
          		$field .="authors " if ($authors eq "");
          		$field .="keywords " if ($keywords eq "");
          		$field .="description " if ($description eq "");
          	  
          	  	print $error_msg ."\t\t expected field: ". $field. " $name,$synopsis,$date,$authors,$keywords,$description \n";
          	  
          }
          else
          {  
          		
          	  print JSON "\n\"$nb_correct_scripts\": ";
	          print JSON &write_sections($name,$synopsis,$date,$authors,$keywords,$description);
	          print $fic," is ok\n";
	          print JSON "," if ($nbfile != $nb_scripts);
	          $nb_correct_scripts++;
          }
}



sub write_sections($$){
    my $name = shift;
    
    my $synopsis = shift;
    my $date = shift;
    my $authors = shift;
    my $keywords = shift;
    my $description = shift;
    
    $name=~s/^\s+//;
    $name=~s/\s+$//;
    $synopsis=~s/^\s+//;
    $synopsis=~s/\s+$//;
    $description=~s/^\s+//;
    $description=~s/\s+$//;
    $description=~s/\t//;
    $date=~s/^\s+//;
    $date=~s/\s+$//;
    $authors=~s/^\s+//;
    $authors=~s/\s+$//;
    $keywords=~s/^\s+//;
    $keywords=~s/\s+$//;

    my $string="";
    $string = "\n{\n\"name\": \"$name\",\n\"synopsis\": \"$synopsis\",\n\"description\": \"$description\",\n\"date\": \"$date\",\n\"authors\": \"$authors\",\n\"keywords\": \"$keywords\"\n}\n";
    
    
    return $string;
    
}


=pod

=head1 NAME

scrips_to_json.pl

=head1 SYNOPSIS

./scripts_to_json.pl [-d DIR] [-f FILE] [-h] [--man]

=head1 DESCRIPTION

Writes a JSON file from a file or a directory passed in parameter.
Works with PERL, PYTHON and BASH scripts.
If your script(s) is/are not well documented, the file wrong_scripts.txt is created and contains bad scripts.
If not, a JSON file containing documentation elements is created.

=head1 AUTHORS

Olivier Rué, Céline Noirot

=head1 VERSION

1

=head1 DATE

05/2013

=head1 KEYWORDS

check documentation scriptotheque

=head1 EXAMPLE

./scripts_to_json.pl -d .

=cut
